import sgMail from '@sendgrid/mail';

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const otpEmailMessage = (otp) => `${otp} is your one time password(OTP) for email verification.`;

const otpEmailTemplate = (otp) => ({
	subject: `${otp} - One Time Password(OTP) Verification`,
	message: otpEmailMessage(otp),
});

const sendMail = async ({ email, subject, message }) => {
	let msg = {
		to: email,
		from: 'info@teknofeet.com',
		subject: subject || `Teknofeet no subject provided`,
		text: message || `Teknofeet no message provided`,
	};
	const res = await sgMail.send(msg).catch((err) => {
		console.log(err);
	});
	return res;
};

export default async (req, res) => {
	const { method } = req;
	switch (method) {
		case 'GET':
			try {
				const { otp, email, action } = req.query;
				switch (action) {
					case 'generateOTP':
						const { subject, message } = otpEmailTemplate(otp);
						const response = await sendMail({
							email,
							subject,
							message,
						});
						res.status(200).json({
							success: true,
							data: { ...response, otp, email },
						});
						break;
					case 'verifyOTP':
						res.status(200).json({ success: false });
						break;
					default:
						res.status(200).json({
							success: false,
							data: 'invalid action',
						});
						break;
				}
			} catch (error) {
				res.status(200).json({ success: false });
			}
			break;
		case 'POST':
			res.status(200).json({
				success: false,
				data: 'nothing designed as of yet',
			});
			break;
		default:
			res.status(200).json({ success: false });
			break;
	}
};
