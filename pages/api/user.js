import axios from 'axios';
import Cors from 'cors';
import initMiddleware from '../../lib/init-middleware';
import { CLIENTFITTINGFILTER, CLIENT } from '../../helpers/query';

const cors = initMiddleware(
	// You can read more about the available options here: https://github.com/expressjs/cors#configuration-options
	Cors({
		// Only allow requests with GET, POST and OPTIONS
		methods: ['GET', 'POST', 'OPTIONS'],
	}),
);

const getGraphql = async ({ query, variables }) => {
	try {
		const response = await axios.get(process.env.TEKNOFEET_GRAPHQL_API_URL + `?query=${query}&variables=${JSON.stringify(variables)}`, {
			headers: {
				Authorization: `Bearer ${process.env.TEKNOFEET_GRAPHQL_API_KEY}`,
				'Content-Type': 'application/json',
			},
		});
		return response.data;
	} catch (error) {
		console.error(error);
	}
	return false;
};

const User = async (req, res) => {
	await cors(req, res);
	const { method } = req;
	switch (method) {
		case 'GET':
			try {
				const { clientID, email, phone_number, action } = req.query;
				switch (action) {
					case 'GETCLIENTID':
						if (email) {
							if (email == 'admin@admin.com') {
								res.status(200).send({
									success: true,
									data: {
										clientID: '0e2bce24-9ec8-4983-88db-b417b760c507',
									},
								});
							} else if (email == 'ahrenpradhan@gmail.com') {
								res.status(200).send({
									success: true,
									data: {
										clientID: '0e2bce24-9ec8-4983-88db-b417b760c581',
										//non existing id
									},
								});
							} else {
								res.status(200).send({
									success: false,
									data: 'Client not found',
								});
							}
						} else if (phone_number) {
							if (phone_number == '9560490133') {
								res.status(200).send({
									success: true,
									data: {
										clientID: '0e2bce24-9ec8-4983-88db-b417b760c507',
									},
								});
							} else {
								res.status(200).send({
									success: false,
									data: 'Client not found',
								});
							}
						} else {
							res.status(200).send({
								success: false,
								data: 'Missing parameters email or phone_number',
							});
						}
						break;
					case 'GETCLIENTFITTINGFILTERDETAILS':
						if (clientID) {
							const response = await getGraphql({
								query: CLIENTFITTINGFILTER,
								variables: {
									clientID,
								},
							});
							res.status(200).send({
								success: true,
								data: response.data,
							});
						} else {
							res.status(200).send({
								success: false,
								data: 'Client ID is missing',
							});
						}
						break;
					case 'GETCLIENTDETAILS':
						if (clientID) {
							const response = await getGraphql({
								query: CLIENT,
								variables: {
									clientID: clientID,
								},
							});
							res.status(200).send({
								success: !!response.data.client,
								data: response.data,
							});
						} else {
							res.status(status).send({ success: false, data: 'Client ID missing' });
						}
						break;
					default:
						res.status(400).send({
							success: false,
							data: 'Action does not exists',
						});
						break;
				}
			} catch (error) {
				res.status(200).send({
					success: false,
					data: 'Internal Error',
				});
			}
			break;
		case 'POST':
			const { valueType, value, action } = req.body;
			if (valueType.length + value.length + action.length > 0) {
				switch (action) {
					// case 'CHECK USER STATUS':
					// 	if(client)
					// if (valueType == 'email') {
					// 	res.status(200).json({
					// 		success: true,
					// 		data: {
					// 			teknofeet: true,
					// 			shopify: true,
					// 		},
					// 	});
					// } else if (valueType == 'phone_number') {
					// 	res.status(200).json({
					// 		success: true,
					// 		data: {
					// 			teknofeet: true,
					// 			shopify: false,
					// 		},
					// 	});
					// } else {
					// 	res.status(200).json({
					// 		success: false,
					// 		data: 'valueType does not exists',
					// 	});
					// }
					// break;
					default:
						res.status(200).json({
							success: false,
							data: 'action does not exists',
						});
						break;
				}
			} else {
				res.status(200).json({
					success: false,
					data: 'Data mmissing',
				});
			}
			break;
		default:
			res.status(400).json({ success: false });
			break;
	}
};

export default User;
