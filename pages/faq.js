// DOCUMENTATION: http://styletron.org

import { Div } from 'atomize';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { setPageMode, setLoading } from '../Stores/pagestore';
import Head from 'next/head';

import FAQ from '../src/faq';
import Footer from '../Components/footer';

export default function FaqPage() {
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch(setPageMode({ pageMode: '/faq' }));
		setTimeout(() => {
			dispatch(setLoading({ loading: false }));
		}, 1500);
		return () => {
			dispatch(setLoading({ loading: true }));
		};
	}, []);
	return (
		<>
			<Head>
				<title>Teknofeet FAQ</title>
				<meta
					name='viewport'
					content='initial-scale=1.0, width=device-width, maximum-scale=1.0, user-scalable=no'
				/>
			</Head>
			<Div className='faq-page'>
				<FAQ />
			</Div>
			<Footer />
		</>
	);
}
