import React, { useEffect } from 'react';
import { useRouter } from 'next/router';

export default function Main() {
	const router = useRouter();
	useEffect(() => {
		if (router.pathname == '/') {
			router.push('/home');
		}
	}, [router]);
	return <></>;
}
