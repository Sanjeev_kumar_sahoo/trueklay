import { useEffect } from 'react';
import { Div } from 'atomize';
import { useDispatch } from 'react-redux';
import { setPageMode,setLoading } from '../../Stores/pagestore';
import Head from 'next/head';

import Profile from '../../src/marketplace/profile';
import Footer from '../../Components/footer';

export default function MarketPlacePage_Profile() {
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch(setPageMode({ pageMode: '/marketplace/profile' }));
		setTimeout(() => {
			dispatch(setLoading({ loading: false }));
		}, 1500);
		return () => {
			dispatch(setLoading({ loading: true }));
		};
	}, []);
	return (
		<>
			<Head>
				<title>My Profile</title>
				<meta
					name='viewport'
					content='initial-scale=1.0, width=device-width, maximum-scale=1.0, user-scalable=no'
				/>
			</Head>
			<Div className='profile-page'>
				<Profile />
			</Div>
			<Footer />
		</>
	);
}
