import { useState, useEffect } from 'react';
import { Div } from 'atomize';
import { useDispatch } from 'react-redux';
import { setPageMode, setLoading } from '../../Stores/pagestore';
import Head from 'next/head';
import { useRouter } from 'next/router';

import MarketPlace from '../../src/marketplace';
import MarketPlaceChoice from '../../src/marketplace/choice';

import Footer from '../../Components/footer';

export default function MarketPlacePage() {
	const dispatch = useDispatch();
	const router = useRouter();
	const [state, setState] = useState({
		filter: false,
		type: null,
	});
	useEffect(() => {
		if (!!router) {
			const { type } = router.query;
			if (type) {
				setState({
					...state,
					filter: true,
					type,
				});
			}
		}
	}, [router]);
	useEffect(() => {
		dispatch(setPageMode({ pageMode: '/marketplace' }));
		setTimeout(() => {
			dispatch(setLoading({ loading: false }));
		}, 1500);
		return () => {
			dispatch(setLoading({ loading: true }));
		};
	}, []);
	return (
		<>
			<Head>
				<title>Marketplace</title>
				<meta
					name='viewport'
					content='initial-scale=1.0, width=device-width, maximum-scale=1.0, user-scalable=0'
				/>
				<meta name='theme-color' content='#1a89f0' />
			</Head>
			<Div
				className='marketplace-page'
				style={
					!state.filter
						? {
								display: 'flex',
								flexDirection: 'column',
								marginBottom: '1em',
						  }
						: {}
				}>
				{state.filter ? <MarketPlace /> : <MarketPlaceChoice />}
			</Div>
			<Footer />
		</>
	);
}
