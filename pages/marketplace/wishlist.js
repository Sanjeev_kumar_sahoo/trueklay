import { useEffect } from 'react';
import { Div } from 'atomize';
import { useDispatch } from 'react-redux';
import { setPageMode, setLoading } from '../../Stores/pagestore';
import Head from 'next/head';

import Wishlist from '../../src/marketplace/wishlist';
import Footer from '../../Components/footer';

export default function MarketPlacePage_Wishlist() {
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch(setPageMode({ pageMode: '/marketplace/wishlist' }));
		setTimeout(() => {
			dispatch(setLoading({ loading: false }));
		}, 1500);
		return () => {
			dispatch(setLoading({ loading: true }));
		};
	}, []);
	return (
		<>
			<Head>
				<title>Wishlist</title>
				<meta
					name='viewport'
					content='initial-scale=1.0, width=device-width, maximum-scale=1.0, user-scalable=no'
				/>
			</Head>
			<Div className='wishlist-page'>
				<Wishlist />
			</Div>
			<Footer />
		</>
	);
}
