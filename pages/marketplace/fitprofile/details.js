import { useEffect } from 'react';
import { Div } from 'atomize';
import { useDispatch } from 'react-redux';
import { setPageMode, setLoading } from '../../../Stores/pagestore';
import Head from 'next/head';

import FitProfileDetails from '../../../src/marketplace/fitprofile/details';
import Footer from '../../../Components/footer';

export default function MarketPlacePage() {
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch(setPageMode({ pageMode: '/marketplace/fitprofile' }));
		setTimeout(() => {
			dispatch(setLoading({ loading: false }));
		}, 1500);
		return () => {
			// dispatch(setLoading({ loading: true }));
		};
	}, []);
	return (
		<>
			<Head>
				<title>Feet Profile</title>
				<meta
					name='viewport'
					content='initial-scale=1.0, width=device-width, maximum-scale=1.0, user-scalable=0'
				/>
				<meta name='theme-color' content='#1a89f0' />
			</Head>
			<Div className='marketplace-page'>
				<FitProfileDetails />
			</Div>
			<Footer />
		</>
	);
}
