// DOCUMENTATION: http://styletron.org

import { Div } from 'atomize';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { setPageMode, setLoading } from '../Stores/pagestore';
import Head from 'next/head';

import LegalDocs from '../src/legaldocs';
import Footer from '../Components/footer';

export default function LegalDocsPage() {
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch(setPageMode({ pageMode: '/legaldocs' }));
		setTimeout(() => {
			dispatch(setLoading({ loading: false }));
		}, 1500);
		return () => {
			dispatch(setLoading({ loading: true }));
		};
	}, []);
	return (
		<>
			<Head>
				<title>Teknofeet</title>
				<meta
					name='viewport'
					content='initial-scale=1.0, width=device-width, maximum-scale=1.0, user-scalable=0'
				/>
			</Head>
			<Div className='legaldocs-page'>
				<LegalDocs />
			</Div>
			<Footer />
		</>
	);
}
