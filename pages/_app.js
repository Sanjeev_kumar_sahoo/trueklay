// import App from 'next/app';
import { Provider as StyletronProvider } from 'styletron-react';
import { StyleReset } from 'atomize';
import { styletron } from '../styletron';

import { Provider } from 'react-redux';
import store from '../Stores';
import NProgress from 'nprogress'; //nprogress module

import Router from 'next/router';
import Header from '../Components/header';
import LoadingScreen from '../Components/common/loadingscreen';

import 'nprogress/nprogress.css'; //styles of nprogress
import '../styles/index.scss';
// import 'flickity/dist/flickity.min.css';

//Binding events.
Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

export default function App({ Component, pageProps }) {
	return (
		<StyletronProvider value={styletron}>
			<StyleReset />
			<Provider store={store}>
				<Header />
				<LoadingScreen />
				<Component {...pageProps} />
			</Provider>
		</StyletronProvider>
	);
}
