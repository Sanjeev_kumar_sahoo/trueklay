const ValidateEmail = (emailAddress) => {
	let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if (emailAddress.match(regexEmail)) {
		return true;
	}
	return false;
};

const ValidatePhoneNumber = (phone_number) => {
	var phoneno = /^\d{10}$/;
	if (phone_number.match(phoneno)) {
		return true;
	}
	return false;
};

export const InputType = (value) => {
	if (ValidateEmail(value)) {
		return 'email';
	}
	if (ValidatePhoneNumber(value)) {
		return 'phone_number';
	}
	return null;
};

export const ValidatePassword = (password) => {
	let lowerCaseLetters = /[a-z]/g;
	var upperCaseLetters = /[A-Z]/g;
	var numbers = /[0-9]/g;
	return {
		lowerCase: !!password.match(lowerCaseLetters),
		upperCase: !!password.match(upperCaseLetters),
		number: !!password.match(numbers),
		passwordLength: password.length > 8,
	};
};

export const CheckPasswordValidation = (value) => {
	let obj = ValidatePassword(value);
	return obj.lowerCase && obj.upperCase && obj.number && obj.passwordLength;
};