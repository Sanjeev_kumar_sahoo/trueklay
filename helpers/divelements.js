import { throttle } from 'lodash';

const isElementInViewport = throttle((el) => {
	// Special bonus for those using jQuery
	if (typeof jQuery === 'function' && el instanceof jQuery) {
		el = el[0];
	}

	var rect = el.getBoundingClientRect();

	return (
		rect.top >= 0 &&
		rect.left >= 0 &&
		rect.bottom <=
			(window.innerHeight ||
				document.documentElement
					.clientHeight) /* or $(window).height() */ &&
		rect.right <=
			(window.innerWidth ||
				document.documentElement.clientWidth) /* or $(window).width() */
	);
}, 100);
const elementInViewport2 = throttle((el, handle) => {
	var top = el.offsetTop;
	var left = el.offsetLeft;
	var width = el.offsetWidth;
	var height = el.offsetHeight;

	while (el.offsetParent) {
		el = el.offsetParent;
		top += el.offsetTop;
		left += el.offsetLeft;
	}
	const value =
		top < window.pageYOffset + window.innerHeight &&
		left < window.pageXOffset + window.innerWidth &&
		top + height > window.pageYOffset &&
		left + width > window.pageXOffset;
	if (handle) {
		handle(value);
	}
	return value;
}, 10);

const hideAddressBar = (win) => {
	var doc = win.document;

	// If there's a hash, or addEventListener is undefined, stop here
	if (!location.hash && win.addEventListener) {
		//scroll to 1
		window.scrollTo(0, 1);
		var scrollTop = 1,
			getScrollTop = function () {
				return (
					win.pageYOffset ||
					(doc.compatMode === 'CSS1Compat' &&
						doc.documentElement.scrollTop) ||
					doc.body.scrollTop ||
					0
				);
			},
			//reset to 0 on bodyready, if needed
			bodycheck = setInterval(function () {
				if (doc.body) {
					clearInterval(bodycheck);
					scrollTop = getScrollTop();
					win.scrollTo(0, scrollTop === 1 ? 0 : 1);
				}
			}, 15);

		win.addEventListener('load', function () {
			setTimeout(function () {
				//at load, if user hasn't scrolled more than 20 or so...
				if (getScrollTop() < 20) {
					//reset to hide addr bar at onload
					win.scrollTo(0, scrollTop === 1 ? 0 : 1);
				}
			}, 0);
		});
	}
}

const deviceType = () => {
	if (window) {
		return window.screen.availWidth > 660 ? 'desktop' : 'mobile';
	}
};

export { elementInViewport2, isElementInViewport, hideAddressBar, deviceType };
