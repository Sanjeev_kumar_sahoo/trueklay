import axios from 'axios';

// phone nnumber
export const generateOTP = async (phone_number) => {
	const response = await axios.get('/api/otp', {
		params: {
			mode: 'generateOTP',
			phone_number,
		},
	});
	return response.data;
};
export const verifyOTP = async (phone_number, session_id, otp_input) => {
	const response = await axios.get('/api/otp', {
		params: {
			mode: 'verifyOTP',
			phone_number,
			session_id,
			otp_input,
		},
	});
	return response.data;
};

// email
export const generateEmailOTP = async (email) => {
	const otp = createOTP();
	const response = await axios.get('/api/mail', {
		params: {
			otp,
			email,
			action: 'generateOTP',
		},
	});
	return response.data;
};

// user
export const getClientID = async ({ email, phone_number }) => {
	const response = await axios.get('/api/user', {
		params: {
			action: 'GETCLIENTID',
			email,
			phone_number,
		},
	});
	return response.data;
};
export const getClientDetails = async (clientID) => {
	const response = await axios.get('/api/user', {
		params: {
			clientID,
			action: 'GETCLIENTDETAILS',
		},
	});
	return response.data;
};
export const getClientFittingFilterDetails = async (clientID) => {
	console.log(clientID);
	const response = await axios.get('/api/user', {
		params: {
			action: 'GETCLIENTFITTINGFILTERDETAILS',
			clientID,
		},
	});
	return response.data;
};

// extra
const createOTP = () => {
	var digits = '0123456789';
	let OTP = '';
	for (let i = 0; i < 6; i++) {
		OTP += digits[Math.floor(Math.random() * 10)];
	}
	return OTP;
};
