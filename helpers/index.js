// 60   3600

export const secondsToClock = (value) => {
	const hr = value / 3600;
	const min = value - 3600 * hr;
	const sec = value - 3600 * hr - 60 * min;
	return `${hr > 0 ? `${hr} :` : ''}${min > 0 ? `${min} : ` : ''}${sec > 0 ? `${sec}` : ''}`;
};

export const convertHMS = (value) => {
	const sec = parseInt(value, 10); // convert value to number if it's string
	let hours = Math.floor(sec / 3600); // get hours
	let minutes = Math.floor((sec - hours * 3600) / 60); // get minutes
	let seconds = sec - hours * 3600 - minutes * 60; //  get seconds
	// add 0 if value < 10; Example: 2 => 02
	if (hours < 10) {
		hours = '0' + hours;
	}
	if (minutes < 10) {
		minutes = '0' + minutes;
	}
	if (seconds < 10) {
		seconds = '0' + seconds;
	}
	return (hours > 0 ? hours + ' : ' : '') + (minutes > 0 ? minutes + ' : ' : '') + seconds; // Return is HH : MM : SS
};

export const handleUserBrowserData = (value, details) => {
	switch (value) {
		case 'STOREUSER':
			localStorage.setItem(
				'teknofeetLoggedInUser',
				JSON.stringify({
					value: details.value,
					password: details.password,
					valueType: details.valueType,
					status: details.status,
					timestamp: details.timestamp,
				}),
			);
			return true;
		case 'GETUSER':
			let teknofeetLoggedInUser = localStorage.getItem('teknofeetLoggedInUser');
			if (teknofeetLoggedInUser) {
				return JSON.parse(teknofeetLoggedInUser);
			}
			return false;
		default:
			console.log(`${value} is not a valid input type.`);
			break;
	}
};
