import React, { useState } from 'react';
import { Div, Icon } from 'atomize';
import ModernDatepicker from 'react-modern-datepicker';

const StringToDate = (value) => {
	if (value) {
		const part = value.split('/');
		return new Date(part[2], part[1] - 1, part[0]);
	}
	return new Date();
};

const customdatepicker = ({ value, handleChange }) => {
	const [startDate, setStartDate] = useState(StringToDate(value));
	return (
		<Div pos='relative'>
			<ModernDatepicker
				className='customDatePicker'
				date={startDate}
				format={'DD/MM/YYYY'}
				showBorder
				// icon={<Icon name='Search' size='20px' />}
				onChange={(date) => {
					setStartDate(date);
				}}
				placeholder={'Select a date'}
				primaryColor={'#1a89f0'}
			/>
			<Div
				pos='absolute'
				right='1em'
				top='0'
				h='100%'
				d='flex'
				flexDir='column'
				justify='center'>
				<Div className='calendar-icon' />
			</Div>
		</Div>
	);
};

export default customdatepicker;
