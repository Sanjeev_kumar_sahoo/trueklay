import React, { useState } from 'react';
import { Div, Text, Button, Icon } from 'atomize';

const CustomQuantityButton = ({ amount, incrementQty, decrementQty }) => {
	// const [qty, setQty] = useState(amount);
	return (
		<Div d='flex' rounded='circle' className='CustomQuantityButton'>
			<Button
				onClick={decrementQty}
				w='fit-content'
				h='100%'
				p={{ x: '0.5em' }}
				bg='transparent'
				// border='1px solid'
				borderColor='black'
				hoverBg='brand'
				hoverTextColor='white'
				hoverBorderColor='brand'
				rounded={{ l: 'circle' }}>
				<Icon name='Minus' size='20px' color='#707070' />
			</Button>
			<Text
				h='fit-content'
				w='fit-content'
				textColor='brand'
				textWeight='bold'
				// border={{ y: '1px solid' }}
				p={{ x: '0.5em' }}
				borderColor='black'>
				{amount}
			</Text>
			<Button
				onClick={incrementQty}
				w='fit-content'
				h='100%'
				p={{ x: '0.5em' }}
				bg='transparent'
				// border='1px solid'
				borderColor='black'
				hoverBg='brand'
				hoverTextColor='white'
				// hoverBorderColor='brand'
				rounded={{ r: 'circle' }}>
				<Icon name='Plus' size='20px' color='#707070' />
			</Button>
		</Div>
	);
};

const BagCard = ({ product, incrementQty, decrementQty, removeProduct }) => {
	return (
		<>
			<Div className='bag-card'>
				<Div className='bag-card-image'>
					<Div
						bgImg='https://assets.myntassets.com/f_webp,dpr_1.5,q_60,w_210,c_limit,fl_progressive/assets/images/12786380/2020/10/20/57e4ead2-0e80-48b4-bee0-cece0f1c68c61603176607927mrwonkerMenMulticolouredSneakers1.jpg'
						bgSize='cover'
						bgPos='center'
						h='100%'
						pos='absolute'
						w='100%'
					/>
				</Div>
				<Div className='bag-card-content'>
					<Div d='flex' justify='space-between'>
						<Text className='bag-card-title'>{product.name}</Text>
						<Button
							onClick={removeProduct}
							h='fit-content'
							w='fit-content'
							bg='#fff'
							p='0px'
							textColor='#3e3e3e'>
							REMOVE
						</Button>
					</Div>
					<Text className='bag-card-description'>
						{product.description}
					</Text>
					<Div flexGrow='1' />
					<Div d='flex' justify='space-between' w='100%'>
						<CustomQuantityButton
							amount={product.selectedQty}
							incrementQty={incrementQty}
							decrementQty={decrementQty}
						/>
						<Text className='bag-card-price'>
							Rs. {product.price.new * product.selectedQty}
						</Text>
					</Div>
				</Div>
			</Div>
			<Div className='bag-card-separator' />
		</>
	);
};

export default BagCard;
