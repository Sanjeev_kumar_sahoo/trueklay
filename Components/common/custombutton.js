import React, { useState } from 'react';
import { Div, Text } from 'atomize';

const SliderButton = ({ mode, setMode }) => {
	return (
		<Div
			className={
				mode
					? 'sliderButton sliderButton-right'
					: 'sliderButton sliderButton-left'
			}
			onClick={setMode}>
			<Div
				className={
					mode ? 'custom-button' : 'custom-button is-selected'
				}>
				<Text>FIT RATE</Text>
			</Div>
			<Div
				className={
					mode ? 'custom-button is-selected' : 'custom-button'
				}>
				<Text>FIT MAP</Text>
			</Div>
		</Div>
	);
};

export { SliderButton };
