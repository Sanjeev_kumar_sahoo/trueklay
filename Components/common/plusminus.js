import React from 'react';
import { Motion, spring, presets } from 'react-motion';

const PlusMinus = ({ toggle }) => {
	const style = {
		overflow: 'visible',
		cursor: 'pointer',
		// width:'1.8em',
		// disable touch highlighting on devices
		WebkitTapHighlightColor: 'rgba(0,0,0,0)',
	};

	return (
		<svg viewBox='0 0 96 96' height='1em' style={style}>
			<Motion
				style={{
					x: spring(toggle ? 1 : 0, presets.wobbly),
					y: spring(toggle ? 0 : 1, presets.wobbly),
				}}>
				{({ x, y }) => (
					<g
						id='navicon'
						fill='none'
						stroke={toggle ? '#1a89f0' : '#131313'}
						strokeWidth='10'
						strokeLinecap='round'
						strokeLinejoin='round'>
						<line
							transform={`translate(${-40 * x},${40 * x}) rotate(${
								(1 - x) * 90
							}, 68, 22)`}
							x1='40'
							x2='150'
							y1='20'
							y2='20'
						/>
						<line
							transform={`translate(0,-5) rotate(0, 1, 74)`}
							opacity={y}
							x1='7'
							x2='130'
							y1='55'
							y2='55'
						/>
					</g>
				)}
			</Motion>
		</svg>
	);
};

export default PlusMinus;
