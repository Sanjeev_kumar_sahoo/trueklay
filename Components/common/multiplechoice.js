import React, { useState } from 'react';
import { Radiobox, Label, Dropdown, Anchor, Div } from 'atomize';
const MenuListMaker = ({ menuList, handleOptionClick }) => {
	return (
		<Div shodow='5' className='basic-dropdown'>
			{menuList
				? menuList.map((name, index) => (
						<Anchor
							key={index}
							onClick={() => {
								handleOptionClick(name);
							}}>
							{name}
						</Anchor>
				  ))
				: ['not option available'].map((name, index) => (
						<Anchor>{name}</Anchor>
				  ))}
		</Div>
	);
};

const BasicDropdown = ({ classes, menuList, value, handleChange }) => {
	const [showDropdown, setShowDropdown] = useState(false);
	const handleOptionClick = (value) => {
		handleChange(value);
		setShowDropdown(!showDropdown);
	};
	return (
		<Dropdown
			isOpen={showDropdown}
			onClick={() => {
				setShowDropdown(!showDropdown);
			}}
			className={classes ? classes : ''}
			menu={
				<MenuListMaker
					menuList={menuList}
					handleOptionClick={handleOptionClick}
				/>
			}>
			{value || 'Click Me'}
		</Dropdown>
	);
};

const MultipleRadioChoice = ({ menuList, value, handleChange }) => {
	const toggleSelectedCount = (_value) => {
		handleChange(_value);
	};
	return (
		<Div d='flex' flexWrap='wrap'>
			{menuList.map((name, index) => (
				<>
					<Label
						key={index}
						align='center'
						w='fit-content'
						m={{ y: '0.5rem', r: '2rem' }}>
						<Radiobox
							activeColor='brand'
							onChange={() => toggleSelectedCount(name)}
							checked={value === name}
							name={name}
						/>
						{name}
					</Label>
				</>
			))}
		</Div>
	);
};

export { BasicDropdown, MultipleRadioChoice };
