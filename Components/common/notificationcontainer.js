import { Notification, Icon, Div } from 'atomize';

const NotificationContainer = ({ primaryColor, secondaryColor, message, icon, isOpen, noTick, manualOnClose, handleIsOpen }) => {
	return (
		<Notification
			bg={secondaryColor || 'success100'}
			textColor={primaryColor || 'success800'}
			isOpen={isOpen}
			onClose={() => {
				if (!manualOnClose) {
					handleIsOpen(false);
				}
			}}
			prefix={
				icon ||
				(!noTick && (
					<Div h='100%' d='flex' justify='center' flexDir='column'>
						<Icon name='Success' color={primaryColor || 'success800'} size='18px' m={{ r: '0.5rem' }} />
					</Div>
				))
			}>
			<Div>{message || 'This is a default message'}</Div>
		</Notification>
	);
};

export default NotificationContainer;
