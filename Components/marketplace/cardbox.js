import React, { useState, useEffect } from 'react';
import { Div, Text, Icon, Button } from 'atomize';
import NotificationContainer from '../common/notificationcontainer';

const NotificationContentCart = ({ handleProductClick, text, image }) => {
	return (
		<Div d='flex' justify='space-between'>
			<Div flexGrow='1'>
				<Text>{text}</Text>
				<Button onClick={handleProductClick} h='24px' rounded='circle' bg='brand'>
					<Text textSize='12px'>DETAILS</Text>
				</Button>
			</Div>
			{image && (
				<Div d='flex' h='100%' justify='center' flexDir='column'>
					<Div
						bgImg={
							image ||
							'https://assets.myntassets.com/f_webp,dpr_1.5,q_60,w_210,c_limit,fl_progressive/assets/images/12786380/2020/10/20/57e4ead2-0e80-48b4-bee0-cece0f1c68c61603176607927mrwonkerMenMulticolouredSneakers1.jpg'
						}
						bgSize='cover'
						bgPos='center'
						h='50px'
						w='50px'
					/>
				</Div>
			)}
		</Div>
	);
};
const NotificationContentWishlist = ({ handleProductClick, toggleWishlist, text, addedToWishlist, image }) => {
	return (
		<Div d='flex' justify='space-between'>
			<Div flexGrow='1'>
				<Text>{text}</Text>
				<Div d='flex'>
					{!addedToWishlist && (
						<Button onClick={toggleWishlist} h='24px' rounded='circle' bg='brand' m={{ r: '0.5em' }}>
							<Text textSize='12px'>UNDO</Text>
						</Button>
					)}
					<Button onClick={handleProductClick} h='24px' rounded='circle' bg='brand'>
						<Text textSize='12px'>DETAILS</Text>
					</Button>
				</Div>
			</Div>
			{image && (
				<Div d='flex' h='100%' justify='center' flexDir='column'>
					<Div
						bgImg={
							image ||
							'https://assets.myntassets.com/f_webp,dpr_1.5,q_60,w_210,c_limit,fl_progressive/assets/images/12786380/2020/10/20/57e4ead2-0e80-48b4-bee0-cece0f1c68c61603176607927mrwonkerMenMulticolouredSneakers1.jpg'
						}
						bgSize='cover'
						bgPos='center'
						h='50px'
						w='50px'
					/>
				</Div>
			)}
		</Div>
	);
};

const CardBox = ({ product, addToCart, added, toggleWishlist, handleProductClick }) => {
	let notificationTimer = null;
	const [notificationState, setNotificationState] = useState({
		isOpen: false,
		content: null,
		primaryColor: null,
		secondaryColor: null,
		extra: null,
	});
	const handleIsOpen = (value) => {
		clearTimeout(notificationTimer);
		notificationTimer = setTimeout(() => {
			setNotificationState({
				...notificationState,
				isOpen: value || false,
			});
		}, 1000);
	};
	const enableNotification = ({ content, primaryColor, secondaryColor, extra }) => {
		setNotificationState({
			...notificationState,
			isOpen: true,
			content,
			primaryColor,
			secondaryColor,
			extra,
		});
	};
	const handleToggleWishlist = () => {
		toggleWishlist();
		enableNotification({
			content: product.addedToWishlist ? 'PRODUCT REMOVED FROM WISHLIST' : 'PRODUCT ADDED TO WISHLIST',
			primaryColor: 'info800',
			secondaryColor: 'info100',
			extra: {
				product,
			},
		});
	};
	const handeleAddToCart = ({ productIndex }) => {
		addToCart();
		enableNotification({
			content: added ? 'PRODUCT ALREADY ADDED TO CART' : 'PRODUCT ADDED TO CART',
			primaryColor: 'info800',
			secondaryColor: 'info100',
			extra: {
				productIndex,
			},
		});
	};
	useEffect(() => {
		if (notificationState.content && notificationState.isOpen == false) {
			setTimeout(() => {
				setNotificationState({
					...notificationState,
					content: null,
					primaryColor: null,
					secondaryColor: null,
					extra: null,
				});
			}, 400);
		}
	}, [notificationState]);
	return (
		<>
			<Div className='marketplace-card' hoverShadow='card-shadow' transition='100ms ease-in-out'>
				<Div className='cart-images' onClick={handleProductClick}>
					<Div bgImg={product.images[0].src} bgSize='cover' bgPos='center' h='100%' pos='absolute' w='100%' />
				</Div>
				<Div className='cart-content'>
					<Div className='wishlist-button-overlay' onClick={handleToggleWishlist.bind(this)} />
					<Div onClick={handleProductClick}>
						<Div className='cart-content-section' d='flex' justify='space-between' p={{ r: '10px' }}>
							<Text className='cart-content-title'>{product.name}</Text>
							<Div className='cart-content-wishlist' onClick={handleToggleWishlist.bind(this)}>
								{product.addedToWishlist ? (
									<Icon name='HeartSolid' color='brand' size='20px' />
								) : (
									<Icon name='Heart' color='#707070' size='20px' />
								)}
							</Div>
						</Div>
						<Div className='cart-content-section'>
							<Text className='cart-content-description'>{product.description}</Text>
							<Text className='cart-content-id'>SKU Code: {product.SKU_code}</Text>
						</Div>
						<Div flexGrow='1' />
						<Div className='cart-content-section'>
							<Text className='cart-content-discount'>({product.price.discount} OFF)</Text>
						</Div>
						<Div className='cart-content-section' d='flex' justify='space-between' p={{ r: '10px' }}>
							<Div className='cart-content-price'>
								<Text className='cart-content-new-price'>Rs. {product.price.new}</Text>
								<Text className='cart-content-old-price'>Rs. {product.price.old}</Text>
							</Div>
							<Div className='cart-content-star-rating'>
								<Text className='cart-content-rating'>{product.rating}</Text>
								<Icon name='StarSolid' size='18px' w='14px' color='warning600' />
							</Div>
						</Div>
					</Div>
				</Div>
				<Div
					className={added ? 'cart-button cart-button-added' : 'cart-button'}
					onClick={handeleAddToCart.bind(this)}
					color={added ? 'brand' : undefined}>
					{added ? 'ADDED' : 'ADD TO BAG'}
				</Div>
			</Div>
			<NotificationContainer
				primaryColor={notificationState.primaryColor}
				secondaryColor={notificationState.secondaryColor}
				isOpen={notificationState.isOpen}
				handleIsOpen={handleIsOpen.bind(this)}
				noTick={true}
				message={
					notificationState.content && (
						<>
							{notificationState.content == 'PRODUCT ADDED TO CART' && (
								<NotificationContentCart
									handleProductClick={handleProductClick}
									text='Product added to cart.'
									image={product.images[0].src}
								/>
							)}
							{notificationState.content == 'PRODUCT ALREADY ADDED TO CART' && (
								<NotificationContentCart
									handleProductClick={handleProductClick}
									text='Product already added to cart.'
									image={product.images[0].src}
								/>
							)}
							{notificationState.content == 'PRODUCT ADDED TO WISHLIST' && (
								<NotificationContentWishlist
									handleProductClick={handleProductClick}
									toggleWishlist={toggleWishlist}
									addedToWishlist={product.addedToWishlist}
									text='Product added to wishlist.'
									image={product.images[0].src}
								/>
							)}
							{notificationState.content == 'PRODUCT REMOVED FROM WISHLIST' && (
								<NotificationContentWishlist
									handleProductClick={handleProductClick}
									toggleWishlist={toggleWishlist}
									addedToWishlist={product.addedToWishlist}
									text='Product removed from cart.'
									image={product.images[0].src}
								/>
							)}
						</>
					)
				}
			/>
		</>
	);
};

export default CardBox;
