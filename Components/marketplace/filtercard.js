import { useState, useEffect, useRef } from 'react';
import { Div, Text, Label, Icon } from 'atomize';
import Checkbox from '../common/checkbox';

const FilterCard = ({ data, onFilterClick }) => {
	return (
		<>
			<Div className='filter-card'>
				<Text className='filter-title'>{data.title || 'no name'}</Text>
				<Div className='filter-content'>
					{data.options.map((option, index) => (
						<Label
							className='filter-option'
							key={`filter-${data.title}-${index}`}>
							<Checkbox
								checked={option.status}
								handleChange={() => {
									onFilterClick(option.name);
								}}
								label={
									<Div
										flexGrow='1'
										d='flex'
										w='100%'
										justify='space-between'>
										<Text className='filter-option-name'>
											{option.name}
										</Text>
										<Div flexGrow='1' />
										<Text className='filter-option-qty'>
											{option.qty}
										</Text>
									</Div>
								}
							/>
						</Label>
					))}
				</Div>
			</Div>
			<Div className='horizontal-separator' />
		</>
	);
};

const MobileFilterCard = ({ data, onFilterClick, opened }) => {
	const [state, setState] = useState({ isOpen: 'true', opacity: 0 });
	const _ref = useRef();
	useEffect(() => {
		setTimeout(() => {
			setState({
				...state,
				isOpen: false,
				height: `${_ref.current?.clientHeight}px`,
				opacity: 1,
			});
		}, 1000);
	}, []);
	const _total = data.options.reduce(
		(acc, option) => (option.status ? acc + 1 : acc),
		0,
	);
	return (
		<Div className='mobile-filter-card'>
			<Div
				d='flex'
				justify='space-between'
				// onClick={handleCollapseClick}
				onClick={setState.bind(this, {
					...state,
					isOpen: !state.isOpen,
				})}>
				<Div d='flex'>
					<Text className='mobile-filter-title'>
						{data.title || 'no name'}
					</Text>
					{_total > 0 && <Text m={{ l: '1em' }}>{_total}</Text>}
				</Div>
				{state.isOpen ? (
					<Icon name='Minus' size='20px' />
				) : (
					<Icon name='Plus' size='20px' />
				)}
			</Div>
			<Div
				className={
					state.isOpen
						? 'mobile-filter-content mobile-filter-content-open'
						: 'mobile-filter-content mobile-filter-content-close'
				}
				h={state.isOpen ? state.height || 'fit-content' : '0px'}
				minH='fit-content'>
				<Div
					style={
						state.opacity == 1
							? {}
							: {
									opacity: state.opacity,
							  }
					}>
					{data.options.map((option, index) => (
						<Label
							className='mobile-filter-option'
							key={`mobile-filter-${data.title}-${index}`}>
							<Checkbox
								checked={option.status}
								handleChange={() => {
									onFilterClick(option.name);
								}}
								label={
									<Div
										flexGrow='1'
										d='flex'
										w='100%'
										justify='space-between'>
										<Text className='mobile-filter-option-name'>
											{option.name}
										</Text>
										<Div flexGrow='1' />
										<Text className='mobile-filter-option-qty'>
											{option.qty}
										</Text>
									</Div>
								}
							/>
						</Label>
					))}
				</Div>
			</Div>
		</Div>
	);
};

export { FilterCard, MobileFilterCard };
