import React, { useState } from 'react';
import { Div, Text, Icon, Button } from 'atomize';

const WishlistCardBox = ({
	product,
	removeFromWishList,
	addToCart,
	handleProductClick,
}) => {
	return (
		<Div
			className='marketplace-card'
			hoverShadow='card-shadow'
			transition='100ms ease-in-out'>
			<Div
				className='card-close-icon'
				pos='absolute'
				right='0'
				top='0'
				m='0.5em'>
				<Button
					onClick={removeFromWishList}
					h='2.5rem'
					w='2.5rem'
					bg='gray500'
					hoverBg='gray600'
					rounded='circle'
					m='0'
					p='2px'
					h='fit-content'
					w='fit-content'
					style={{ zIndex: '1' }}>
					<Icon name='Cross' size='20px' color='white' />
				</Button>
			</Div>
			<Div className='cart-images' onClick={handleProductClick}>
				<Div
					// bgImg='https://assets.myntassets.com/f_webp,dpr_1.5,q_60,w_210,c_limit,fl_progressive/assets/images/12786380/2020/10/20/57e4ead2-0e80-48b4-bee0-cece0f1c68c61603176607927mrwonkerMenMulticolouredSneakers1.jpg'
					bgImg={product.images[0].src}
					bgSize='cover'
					bgPos='center'
					h='100%'
					pos='absolute'
					w='100%'
				/>
			</Div>
			<Div className='cart-content' onClick={handleProductClick}>
				<Div
					className='cart-content-section'
					d='flex'
					justify='space-between'
					p={{ r: '10px' }}>
					<Text className='cart-content-title'>{product.name}</Text>
				</Div>
				<Div className='cart-content-section'>
					<Text className='cart-content-description'>
						Men Grey sneakers
					</Text>
					<Text className='cart-content-id'>
						SKU Code: {product.SKU_code}
					</Text>
				</Div>
				<Div flexGrow='1' />
				<Div className='cart-content-section'>
					<Text className='cart-content-discount'>
						({product.discount} OFF)
					</Text>
				</Div>
				<Div
					className='cart-content-section'
					d='flex'
					justify='space-between'
					p={{ r: '10px' }}>
					<Div className='cart-content-price'>
						<Text className='cart-content-new-price'>
							Rs. {product.price.new}
						</Text>
						<Text className='cart-content-old-price'>
							Rs. {product.price.old}
						</Text>
					</Div>
					<Div className='cart-content-star-rating'>
						<Text className='cart-content-rating'>
							{product.rating}
						</Text>
						<Icon
							name='StarSolid'
							size='18px'
							w='14px'
							color='warning600'
						/>
					</Div>
				</Div>
			</Div>
			<Div className='cart-button' onClick={addToCart}>
				MOVE TO BAG
			</Div>
		</Div>
	);
};

export default WishlistCardBox;
