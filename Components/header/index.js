import React, { useState, useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { ThemeProvider, Div, Text, Button, Icon } from 'atomize';
import { setPageOnTop } from '../../Stores/pagestore';
import {
	toggleCart,
	toggleFilter,
	toggleFilterValue,
	handleProduct,
} from '../../Stores/shopifystore';
import { handleUserBrowserData } from '../../helpers';

import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';

import Hamburger from '../common/hamburger';
import PopUp from '../common/popup';

// import PerfectScrollbar from 'react-perfect-scrollbar';
import BagCard from '../common/bagcard';
import { MobileFilterCard } from '../marketplace/filtercard';

const theme = {
	shadows: {
		'new-shadow': '0 8px 12px -2px rgba(0, 0, 0, 0.04)',
		'dropdown-shadow': '0px 2px 4px 4px rgba(0, 0, 0, 0.08)',
	},
	colors: {
		brand: '#1a89f0',
		brandHover: '#ffc5c5',
	},
};

const headerLine = [
	'/marketplace/wishlist',
	'/marketplace/profile',
	'/marketplace/product',
	'/legaldocs',
	'/faq',
];

const CartProductNumber = ({ cart }) => {
	return (
		cart.length > 0 && (
			<Div
				pos='absolute'
				right='-4px'
				top='-4px'
				border='1px solid'
				borderColor='brand'
				bg='brand'
				p='2px'
				rounded='circle'
				d='flex'
				flexDir='column'
				justify='center'>
				<Text textColor='#fff' textSize='12px'>
					{cart.reduce((acc, product) => acc + product.qty, 0)}
				</Text>
			</Div>
		)
	);
};

const linkClassName = (classname, status) =>
	status ? `${classname} ${classname}--selected` : classname;

const HomeNavbar = () => {
	return (
		<ul>
			<li>
				<Link href='/marketplace'>
					<a>HOW IT WORKS</a>
				</Link>
			</li>
			<li>
				<a>KIOSK LOCATOR</a>
			</li>
		</ul>
	);
};
const HomeMobileNav = () => {
	return (
		<Div className='header-navigation-list'>
			<Div className='header-navigation-list-item header-navigation-list-item-header'>
				MENU
			</Div>
			<Link href='/marketplace'>
				<Div className={linkClassName('header-navigation-list-item')}>
					<a>HOW IT WORKS</a>
				</Div>
			</Link>
			<Link href='/marketplace'>
				<Div className={linkClassName('header-navigation-list-item')}>
					<a>KIOSK LOCATOR</a>
				</Div>
			</Link>
			<Link href='/marketplace'>
				<Div className={linkClassName('header-navigation-list-item')}>
					<a>About Truklay</a>
				</Div>
			</Link>
			<Link href='/marketplace'>
				<Div className={linkClassName('header-navigation-list-item')}>
					<a>Contact Us</a>
				</Div>
			</Link>
			<Link href='/marketplace'>
				<Div className={linkClassName('header-navigation-list-item')}>
					<Link href='/faq'>
						<a>Faq</a>
					</Link>
				</Div>
			</Link>
			<Link href='/legaldocs?docs=PRIVACY+POLICY'>
				<Div className={linkClassName('header-navigation-list-item')}>
					<a>Privacy Policy</a>
				</Div>
			</Link>
			<Link href='/legaldocs?docs=LEGAL+DISCLAIMER'>
				<Div className={linkClassName('header-navigation-list-item')}>
					<a>Legal Disclaimer</a>
				</Div>
			</Link>
			<Link href='/legaldocs?docs=TERMS+CONDITIONS'>
				<Div className={linkClassName('header-navigation-list-item')}>
					<a>Terms & Conditions</a>
				</Div>
			</Link>
			<Div d='flex' m='0px auto'>
				<Div className='facebook-nav-logo nav-footer-icon'></Div>
				<Div className='instagram-nav-logo nav-footer-icon'></Div>
				<Div className='linkedIn-nav-logo nav-footer-icon'></Div>
			</Div>
		</Div>
	);
};

const MarketNavbar = () => {
	const router = useRouter();
	const [currentPath, setCurrentPath] = useState(null);
	const [profile, setProfile] = useState(false);
	const shopifyStore = useSelector((state) => state.shopifyStore);
	const dispatch = useDispatch();
	const handleProfile = (value = 0) => {
		if (!shopifyStore.isCartOpen) {
			setProfile(typeof value === 'boolean' ? value : !profile);
		}
		if (typeof value !== 'boolean') {
			if (shopifyStore.isCartOpen) {
				dispatch(toggleCart());
			}
			setProfile(!profile);
		}
	};
	const handleLogout = () => {
		handleUserBrowserData('STOREUSER', {
			...handleUserBrowserData('GETUSER'),
			status: 'LOGOUT',
		});
		router.push('/home');
	};
	useEffect(() => {
		return () => {
			if (shopifyStore.isCartOpen) {
				dispatch(toggleCart());
			}
		};
	}, []);
	useEffect(() => {
		setCurrentPath(router.pathname);
	}, [router]);
	return (
		<>
			<ul className='marketplace-navbar'>
				<li
					onMouseLeave={() => {
						handleProfile(false);
					}}
					onMouseEnter={() => {
						handleProfile(true);
					}}
					onClick={() => {
						handleProfile();
					}}>
					<a>
						<Div className='profile-icon marketplace-icon' />
						Profile
					</a>
					{profile && (
						<Div
							className='marketplace-profile-dropdown'
							shadow='dropdown-shadow'>
							<Text className='marketplace-profile-dropdown-user-name'>
								Hello Karan
							</Text>
							<Text className='marketplace-profile-dropdown-user-id'>
								USFID 1120420BB
							</Text>
							<Link href='/marketplace/fitprofile'>
								<Text
									className={linkClassName(
										'marketplace-profile-dropdown-button',
									)}>
									<a>My Fit Profile</a>
								</Text>
							</Link>
							<Div className='horizontal-separator' />
							<Link href='/marketplace/profile'>
								<Text
									className={linkClassName(
										'marketplace-profile-dropdown-button',
										currentPath.includes(
											'/marketplace/profile',
										),
									)}>
									<a>My Profile</a>
								</Text>
							</Link>
							<Text
								className={linkClassName(
									'marketplace-profile-dropdown-button',
								)}>
								<a>Orders</a>
							</Text>
							<Link href='/marketplace/wishlist'>
								<Text
									className={linkClassName(
										'marketplace-profile-dropdown-button',
										currentPath.includes(
											'/marketplace/wishlist',
										),
									)}>
									<a>Wishlist</a>
								</Text>
							</Link>
							<Div className='horizontal-separator' />
							<Text
								className={linkClassName(
									'marketplace-profile-dropdown-button',
								)}>
								<a>Truklay Credits</a>
							</Text>
							<Text
								className={linkClassName(
									'marketplace-profile-dropdown-button',
								)}>
								<a>Saved Cards</a>
							</Text>
							<Text
								className={linkClassName(
									'marketplace-profile-dropdown-button',
								)}>
								<a>Saved Addresses</a>
							</Text>
							<Div className='horizontal-separator' />
							<Text
								className={linkClassName(
									'marketplace-profile-dropdown-button',
								)}>
								<a>Contact Us</a>
							</Text>
							<Text
								className={linkClassName(
									'marketplace-profile-dropdown-button',
								)}
								onClick={handleLogout.bind(this)}>
								<a>Logout</a>
							</Text>
						</Div>
					)}
				</li>
				<li
					className={
						currentPath?.includes('/marketplace/wishlist')
							? 'marketplace-navbar-icon-active'
							: undefined
					}>
					<Link href='/marketplace/wishlist'>
						<a>
							<Div className='wishlist-icon marketplace-icon' />
							Wishlist
						</a>
					</Link>
				</li>
				<li
					onClick={() => {
						dispatch(toggleCart());
					}}
					className={
						shopifyStore.isCartOpen
							? 'marketplace-navbar-icon-active'
							: ''
					}>
					<a>
						<Div
							className='cart-icon marketplace-icon'
							pos='relative'>
							<CartProductNumber cart={shopifyStore.cart} />
						</Div>
						Bag
					</a>
				</li>
			</ul>
		</>
	);
};
const MakretplaceMobileNav = () => {
	const router = useRouter();
	const shopifyStore = useSelector((state) => state.shopifyStore);
	const dispatch = useDispatch();
	const handleLogout = () => {
		handleUserBrowserData('STOREUSER', {
			...handleUserBrowserData('GETUSER'),
			status: 'LOGOUT',
		});
		router.push('/home');
	};
	return (
		<Div className='header-navigation-list'>
			<Div className='header-navigation-list-item header-navigation-list-item-header'>
				MENU
			</Div>
			<Link href='/'>
				<Div className={linkClassName('header-navigation-list-item')}>
					<a>My Fit Profile</a>
				</Div>
			</Link>
			<Link href='/marketplace/wishlist'>
				<Div className={linkClassName('header-navigation-list-item')}>
					<a>WishList</a>
				</Div>
			</Link>
			<Div
				className={linkClassName('header-navigation-list-item')}
				onClick={() => {
					dispatch(toggleCart());
				}}>
				<a>Bag</a>
			</Div>
			<Link href='/marketplace/profile'>
				<Div className={linkClassName('header-navigation-list-item')}>
					<a>My Profile</a>
				</Div>
			</Link>
			<Link href='/'>
				<Div className={linkClassName('header-navigation-list-item')}>
					<a>Orders</a>
				</Div>
			</Link>
			<Link href='/'>
				<Div className={linkClassName('header-navigation-list-item')}>
					<a>Saved Addresses</a>
				</Div>
			</Link>
			<Link href='/'>
				<Div className={linkClassName('header-navigation-list-item')}>
					<a>Contact Us</a>
				</Div>
			</Link>
			<Div
				className={linkClassName('header-navigation-list-item')}
				onClick={handleLogout.bind(this)}>
				<a>Logout</a>
			</Div>
			<Div flexGrow='1' />
			<Div d='flex' m='0px auto'>
				<Div className='facebook-nav-logo nav-footer-icon'></Div>
				<Div className='instagram-nav-logo nav-footer-icon'></Div>
				<Div className='linkedIn-nav-logo nav-footer-icon'></Div>
			</Div>
		</Div>
	);
};

const BagNav = ({ bagRef, shopifyStore, dispatch, router }) => {
	return (
		<>
			<Div
				ref={bagRef}
				onMouseLeave={() => {
					if (shopifyStore.isCartOpen) {
						dispatch(toggleCart());
					}
				}}
				className={
					shopifyStore.isCartOpen
						? 'bag-container bag-container-open'
						: 'bag-container bag-container-close'
				}
				shadow='3'>
				{shopifyStore.cart.length == 0 ? (
					<Div
						h='100%'
						w='100%'
						d='flex'
						flexDir='column'
						justify='center'
						align='center'>
						<Div className='empty-bag-icon marketplace-empty-bag-icon' />
						<Div m='1em' d='flex' flexDir='column' align='center'>
							<Text
								textSize='18px'
								textColor='brand'
								m='0.5em'
								textWeight='600'
								style={{ letterSpacing: '1px' }}>
								Your bag is empty!
							</Text>
							<Text>
								Add items that you have saved in your wishlist
							</Text>
							<Button
								onClick={() => {
									if (
										!router.pathname.includes('/wishlist')
									) {
										router.push('/marketplace/wishlist');
									}
									dispatch(toggleCart());
								}}
								rounded='circle'
								border='1px solid'
								borderColor='brand'
								h='40px'
								w='16em'
								bg='#fff'
								textColor='brand'
								m={{ y: '1em' }}>
								MY WISHLIST
							</Button>
						</Div>
						<Div m='1em' d='flex' flexDir='column' align='center'>
							<Text>Or check out other new additions!</Text>
							<Button
								onClick={() => {
									if (router.pathname != '/marketplace') {
										router.push('/marketplace');
									}
									dispatch(toggleCart());
								}}
								rounded='circle'
								border='1px solid'
								borderColor='brand'
								h='40px'
								w='16em'
								bg='#fff'
								textColor='brand'
								m={{ t: '1em' }}>
								BACK TO RESULTS
							</Button>
						</Div>
					</Div>
				) : (
					<>
						<Div
							className='bag-section'
							w='100%'
							d='flex'
							justify='space-between'>
							<Text className='bag-section-title'>Your Cart</Text>
							<Button
								onClick={() => {
									dispatch(toggleCart());
								}}
								p='0'
								m='0'
								h='fit-content'
								rounded='circle'
								bg='transparent'>
								<Icon name='Close' size='24px' />
							</Button>
						</Div>
						<Div className='horizontal-separator-thick' />
						<Div className='bag-section bag-card-container'>
							{shopifyStore.cart.map((product) => (
								<BagCard
									key={product.id}
									product={{
										...shopifyStore.filteredProducts.filter(
											(f) => f.id == product.id,
										)[0],
										selectedQty: product.qty,
									}}
									incrementQty={() => {
										dispatch(
											handleProduct({
												id: product.id,
												actionType: 'ADD',
											}),
										);
									}}
									decrementQty={() => {
										dispatch(
											handleProduct({
												id: product.id,
												actionType: 'SUBTRACT',
											}),
										);
									}}
									removeProduct={() => {
										dispatch(
											handleProduct({
												id: product.id,
												actionType:
													'SELECT_REMOVABLE_PRODUCT',
											}),
										);
									}}
								/>
							))}
						</Div>
						<Div flexGrow='1' />
						<Div className='horizontal-separator-thick' />
						<Div
							className='bag-section'
							d='flex'
							flexDir='column'
							justify='center'
							align='center'
							w='100%'>
							<Div
								className='bag-section-title2'
								w='100%'
								d='flex'
								justify='space-between'>
								<Text>SUB-TOTAL</Text>
								<Text>
									Rs.
									{shopifyStore.cart
										.map(
											(d) =>
												shopifyStore.filteredProducts.filter(
													(f) => f.id == d.id,
												)[0].price.new * d.qty,
										)
										.reduce((p, acc) => acc + p, 0)}
								</Text>
							</Div>
							<Text m={{ b: '12px' }} textColor='#707070'>
								Tax included and shipping calculated at checkout
							</Text>
							<Button
								bg='brand'
								hoverBg='white'
								hoverTextColor='brand'
								border='1px solid'
								borderColor='brand'
								w='100%'
								h='40px'
								rounded='circle'>
								PROCEED TO CHECKOUT
							</Button>
						</Div>
					</>
				)}
			</Div>
			{shopifyStore.currentRemovableProduct && (
				<PopUp
					open={shopifyStore.currentRemovableProduct}
					popupHeader={
						<Text
							textWeight='700'
							textSize='18px'
							style={{ letterSpacing: '1px' }}>
							REMOVE PRODUCT
						</Text>
					}
					popupContent={
						<Div d='flex' p={{ x: '2em', t: '1em' }}>
							<Div w='6em' h='6em'>
								<Div
									bgImg='https://assets.myntassets.com/f_webp,dpr_1.5,q_60,w_210,c_limit,fl_progressive/assets/images/12786380/2020/10/20/57e4ead2-0e80-48b4-bee0-cece0f1c68c61603176607927mrwonkerMenMulticolouredSneakers1.jpg'
									bgSize='cover'
									bgPos='center'
									h='100%'
									w='100%'
								/>
							</Div>
							<Div
								flexGrow='1'
								d='flex'
								w='100%'
								justify='center'>
								<Text>
									Are you sure you want to remove this product
									?
								</Text>
							</Div>
						</Div>
					}
					popupFooter={
						<Div d='flex'>
							<Div flexGrow='1' className='modal-div-remover' />
							<Button
								className='modal-footer-secondary-button'
								onClick={() => {
									dispatch(
										handleProduct({
											id:
												shopifyStore
													.currentRemovableProduct.id,
											actionType: 'REMOVE',
										}),
									);
								}}
								bg='#fff'
								border='1px solid'
								borderColor='brand'
								textColor='brand'
								rounded='circle'
								h='40px'>
								REMOVE
							</Button>
							<Button
								className='modal-footer-primary-button'
								onClick={() => {
									dispatch(
										handleProduct({
											id:
												shopifyStore
													.currentRemovableProduct.id,
											actionType: 'ADD_TO_WISHLIST',
										}),
									);
									dispatch(
										handleProduct({
											id:
												shopifyStore
													.currentRemovableProduct.id,
											actionType: 'REMOVE',
										}),
									);
								}}
								bg='brand'
								border='1px solid'
								borderColor='brand'
								textColor='#fff'
								rounded='circle'
								h='40px'
								m={{ l: '1em' }}>
								ADD TO WISHLIST
							</Button>
						</Div>
					}
					onClose={() => {
						dispatch(
							handleProduct({
								id: shopifyStore.currentRemovableProduct.id,
								actionType: 'ABORT_REMOVE',
							}),
						);
					}}
					popupType='modal-bottom'
				/>
			)}
		</>
	);
};

const FilterNav = ({ shopifyStore, dispatch }) => {
	const onFilterClick = (title, name) => {
		dispatch(toggleFilterValue({ title, name }));
	};
	const [openState, setOpenState] = useState(
		shopifyStore.filters.map((a) => false),
	);
	return (
		<Div
			className={
				shopifyStore.isFilterOpen
					? 'filter-container filter-container-open'
					: 'filter-container filter-container-close'
			}>
			{shopifyStore.filters.map((data, index) => (
				<MobileFilterCard
					data={data}
					// opened={openState[index]}
					onFilterClick={onFilterClick.bind(this, data.title)}
					key={`filterCard${index}`}
				/>
			))}

			<Div className='filter-container-bottom-bar-overlay' />
			<Div className='filter-container-bottom-bar'>
				<Button className='filter-container-secondary-button'>
					RESET ALL FILTERS
				</Button>
				<Button className='filter-container-primary-button'>
					APPLY
				</Button>
			</Div>
		</Div>
	);
};

export default function Header() {
	const router = useRouter();
	const dispatch = useDispatch();
	const navRef = useRef();
	const bagRef = useRef();
	const [headerline, setHeaderline] = useState(false);
	const [nav, setNav] = useState(false);
	const [onTop, setOnTop] = useState(true);
	const [mode, setMode] = useState(null);
	const pageStore = useSelector((state) => state.pageStore);
	const shopifyStore = useSelector((state) => state.shopifyStore);
	const scrollTop = () => {
		window.scrollTo({
			top: 0,
			behavior: 'smooth',
		});
	};
	const handleScroll = () => {
		if (scrollY != onTop) {
			setOnTop(!scrollY);
		}
	};
	useEffect(() => {
		window.addEventListener('scroll', handleScroll.bind(this));
		return () => {
			window.removeEventListener('scroll', handleScroll.bind(this));
			if (shopifyStore.isCartOpen) {
				dispatch(toggleCart());
			}
		};
	}, []);
	useEffect(() => {
		handleScroll();
		setNav(false);
		// if (router.pathname.includes('/home')) {
		// 	setMode('home');
		// }
		if (router.pathname.includes('/marketplace')) {
			setMode('marketplace');
		} else {
			setMode('home');
		}
		setHeaderline(headerLine.includes(router.pathname));
	}, [router]);
	useEffect(() => {
		dispatch(setPageOnTop({ pageOnTop: onTop }));
	}, [onTop]);
	useEffect(() => {
		if (nav) {
			disableBodyScroll(navRef);
			if (shopifyStore.isCartOpen) {
				dispatch(toggleCart());
			}
		} else {
			enableBodyScroll(navRef);
		}
		return () => {
			disableBodyScroll(navRef);
		};
	}, [nav]);
	useEffect(() => {
		if (shopifyStore.isCartOpen || shopifyStore.isFilterOpen) {
			setNav(false);
		}
	}, [shopifyStore]);
	return (
		<ThemeProvider theme={theme}>
			{router?.pathname.includes('/marketplace/fitprofile') ? (
				<></>
			) : (
				<>
					<Div
						className={
							router.pathname.includes('/marketplace/product') ||
							router.pathname.includes('/marketplace/profile') ||
							router.pathname.includes('/marketplace/wishlist') ||
							shopifyStore.isCartOpen ||
							shopifyStore.isFilterOpen
								? 'header-main header-main-product'
								: 'header-main'
						}
						border={{ b: '1px solid' }}
						borderColor={headerline ? '#e3e3e3' : 'transparent'}
						shadow={!onTop && 'new-shadow'}
						transition='300ms ease'>
						<a
							className='truklay-logo truklay-size'
							onClick={() => {
								if (
									// router.pathname !== '/home' &&
									router.pathname !== '/marketplace'
								) {
									if (
										router.pathname.includes('/marketplace')
									) {
										router.push('/marketplace');
									} else {
										if (pageStore.logoButton) {
											pageStore.logoButton();
										} else {
											router.push('/home');
										}
									}
								}
								if (shopifyStore.isCartOpen) {
									dispatch(toggleCart());
								}
								if (shopifyStore.isFilterOpen) {
									dispatch(toggleFilter());
								}
								if (nav) {
									setNav(false);
								}
							}}
						/>
						{(router.pathname.includes('/marketplace/product') ||
							router.pathname.includes('/marketplace/profile') ||
							router.pathname.includes('/marketplace/wishlist') ||
							shopifyStore.isCartOpen ||
							shopifyStore.isFilterOpen) && (
							<Div className='header-marketplace-product'>
								<Div
									d='flex'
									onClick={() => {
										if (shopifyStore.isCartOpen) {
											dispatch(toggleCart());
										} else if (shopifyStore.isFilterOpen) {
											dispatch(toggleFilter());
										} else {
											router.push('/marketplace');
										}
									}}>
									<Icon name='LeftArrow' size='24px' />
									<Text className='header-marketplace-product-content'>
										{shopifyStore.isCartOpen ? (
											'SHOPPING BAG'
										) : shopifyStore.isFilterOpen ? (
											'FILTERS'
										) : (
											<>
												{router.pathname.includes(
													'/marketplace/product',
												) && 'BACK'}
												{router.pathname.includes(
													'/marketplace/profile',
												) && 'PROFILE'}
												{router.pathname.includes(
													'/marketplace/wishlist',
												) && 'WISHLIST'}
											</>
										)}
									</Text>
								</Div>
							</Div>
						)}
						<Div className='header-mobile-nav' flexGrow='1' />
						<Div className='header-nav'>
							{mode == 'home' && <HomeNavbar />}
							{mode == 'marketplace' && <MarketNavbar />}
						</Div>
						<Div
							className='header-mobile-nav'
							flexGrow='unset !important'>
							<Div
								d={
									router.pathname.includes('/marketplace') &&
									(shopifyStore.isCartOpen ||
										shopifyStore.isFilterOpen)
										? 'none'
										: 'flex'
								}>
								{router.pathname == '/marketplace' && (
									<Div
										onClick={() => {
											dispatch(toggleFilter());
										}}
										className='funnel-filter-icon marketplace-mobile-icon'
									/>
								)}
								{router.pathname.includes('/marketplace') && (
									<Div
										onClick={() => {
											dispatch(toggleCart());
										}}
										className='cart-icon marketplace-mobile-icon'
										pos='relative'>
										<CartProductNumber
											cart={shopifyStore.cart}
										/>
									</Div>
								)}
								<Div
									className='hamburger'
									onClick={() => {
										setNav(!nav);
									}}>
									<Hamburger toggle={nav} />
								</Div>
							</Div>
							{/* <PerfectScrollbar
						containerRef={(el) => {
							navRef.current = el;
						}}> */}
							<Div
								ref={navRef}
								className={
									nav
										? 'header-navigation-container nav-open'
										: 'header-navigation-container nav-close'
								}>
								{mode == 'home' && <HomeMobileNav />}
								{mode == 'marketplace' && (
									<MakretplaceMobileNav />
								)}
							</Div>
							{/* </PerfectScrollbar> */}
						</Div>
					</Div>
					<Div className='header-overlay' />
					{!pageStore.pageOnTop && (
						<Button
							pos='fixed'
							right='0'
							bottom='0'
							m='2rem'
							onClick={scrollTop}
							bg='white'
							p='0px'
							h='2rem'
							w='2rem'
							rounded='circle'
							style={{ opacity: '0.8', zIndex: '2' }}
							suffix={
								<Icon
									name='UpArrowSolid'
									size='4rem'
									color='#173458'
								/>
							}
						/>
					)}
					<BagNav
						bagRef={bagRef}
						shopifyStore={shopifyStore}
						dispatch={dispatch}
						router={router}
					/>
					{router.pathname == '/marketplace' && (
						<FilterNav
							shopifyStore={shopifyStore}
							dispatch={dispatch}
						/>
					)}
				</>
			)}
		</ThemeProvider>
	);
}
