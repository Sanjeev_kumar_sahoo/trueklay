import { useState, useEffect, useRef } from 'react';
import { Div, Text, Collapse } from 'atomize';
import { faq } from '../../assets/data';
import PlusMinus from '../../Components/common/plusminus';

const addingHtmlNewLine = (x) => {
	let temp = x.split('\n');
	return (
		<Text>
			{temp.map((t, index) => (
				<>
					{t}
					{index != temp.length - 1 && <br />}
				</>
			))}
		</Text>
	);
};

const QuestionContainer = ({ data }) => {
	const [state, setState] = useState({ data, isOpen: 'true', opacity: 0 });
	const _ref = useRef();
	useEffect(() => {
		setTimeout(() => {
			setState({
				...state,
				isOpen: false,
				height: `${_ref.current?.clientHeight}px`,
				opacity: 1,
			});
		}, 1000);
	}, []);
	return (
		<Div
			className='faq-card disableSelection'
			onClick={setState.bind(this, {
				...state,
				isOpen: !state.isOpen,
			})}
			d='flex'>
			<Div
				d='flex'
				flexDir='column'
				// justify='center'
				align='center'
				p={{ x: '1em', t: '0.2em' }}>
				<PlusMinus toggle={state.isOpen} />
			</Div>
			<Div flexGrow='1' pos='relative'>
				<Div className='faq-card-button'>
					<Text className='faq-card-question'>
						{state.data.question}
					</Text>
				</Div>
				<Div
					ref={_ref}
					h={state.isOpen ? 'fit-content' : '0px'}
					minH='fit-content'
					className={
						'faq-card-content ' +
						(state.isOpen
							? 'faq-card-content-open'
							: 'faq-card-content-close')
					}>
					<Div
						className='faq-card-answer'
						style={
							state.opacity == 1
								? {}
								: {
										opacity: state.opacity,
										// position: 'absolute',
										// top: '100vh',
								  }
						}>
						{typeof state.data.answer == 'string' ? (
							addingHtmlNewLine(state.data.answer)
						) : (
							<>
								{state.data.excludeFirstpointFromList &&
									addingHtmlNewLine(state.data.answer[0])}
								<ul>
									{state.data.answer
										.slice(
											state.data.excludeFirstpointFromList
												? 1
												: 0,
										)
										.map((a, index) => (
											<li key={index}>
												<Text>{a}</Text>
											</li>
										))}
								</ul>
							</>
						)}
					</Div>
				</Div>
			</Div>
		</Div>
	);
};

const FAQ = () => {
	const [reload, setReload] = useState(false);
	return (
		<Div className='faq-container'>
			<Div className='faq-section' align='center'>
				<Text
					textAlign='center'
					textSize='22px'
					textWeight='600'
					m={{ b: '1em' }}>
					Frequently Asked Questions
				</Text>
				<Text textAlign='center' m={{ b: '4em' }}>
					Can’t find your answers? Call us at <a>+91-9810001234</a> or
					write to us at <a>service@teknofeet.com</a>
				</Text>
			</Div>
			<Div className='faq-section'>
				<Text
					textColor='#3e3e3e'
					textSize='16px'
					textWeight='600'
					m={{ b: '1em' }}>
					TECHNOLOGY
				</Text>
				{!reload && faq.map((d) => <QuestionContainer data={d} />)}
			</Div>
		</Div>
	);
};

export default FAQ;
