import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { ThemeProvider, Div, Button, Text } from 'atomize';
import { useRouter } from 'next/router';

const theme = {
	colors: {
		brand: '#1a89f0',
	},
};

const CommonButton = ({ name, active, handleClick }) => {
	return (
		<Button
			className={
				active
					? 'legaldocs-top-nav-button legaldocs-top-nav-button-active'
					: 'legaldocs-top-nav-button'
			}
			onClick={handleClick}
			hoverShadow='2'>
			{name}
		</Button>
	);
};

const ArchTypeContent = () => {
	return (
		<Div className='marketplace-fitprofile-archtype'>
			<Text>
				The arch of the foot refers to the area between the ball and the
				heel, along the bottom of the foot. Made up of three different
				arches - the medial longitudinal arch that runs from the end of
				the heel to the ball of the foot along the centre of the foot,
				the lateral longitudinal arch that runs along the outside edge
				of the foot, and the anterior transverse that runs from side to
				side, the arch of the foot provides support to the whole body
				during the day-to-day movement. Before getting any footwear, it
				is highly imperative to know your arch type as it helps in
				making an informed decision about the kind of footwear to choose
				that would support your foot arch.
			</Text>
			<Text>There are three kinds of arches:</Text>
			<ul>
				<li>
					<Text>LOW ARCH/ FLAT FEET</Text>
					<Div className='archImages'>
						<Div className='image-container'>
							<Div
								className='image1'
								bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/Image_1_2x_07735231-66bf-4e82-ac6d-3db141f3ac23.png?v=1614932896'
							/>
						</Div>
						<Div className='image-container'>
							<Div
								className='image2'
								bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/Group_8334_2x_3d927dd7-b08f-459f-afb4-1d905e0787d5.png?v=1614932894'
							/>
						</Div>
						<Div className='image-container'>
							<Div
								className='image3'
								bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/Image_4_2x_3d9c66ad-eb6b-454d-bc03-a905bfcdb64f.png?v=1614932896'
							/>
						</Div>
					</Div>
					<Text>
						In the low arch, the foot has little arch definition and
						higher flexibility than the usual.
					</Text>
					<Text>
						This leads to the foot rolling in excessively while
						walking or running. People with low arch should opt for
						shoes with a straight last and motion control as they
						have rigid devices in the arch area of the shoes for
						increased stability to prevent excessive rolling in of
						the feet.
					</Text>
				</li>
				<li>
					<Text>NORMAL ARCH</Text>
					<Div className='archImages'>
						<Div className='image-container'>
							<Div
								className='image1'
								bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/Image_2_2x_8eb4c19a-6f84-476d-b229-2cd674f4f41f.png?v=1614932896'
							/>
						</Div>
						<Div className='image-container'>
							<Div
								className='image2'
								bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/Group_8335_2x_8aeb7703-6860-43d4-ba52-42356ec1a77e.png?v=1614932894'
							/>
						</Div>
						<Div className='image-container'>
							<Div
								className='image3'
								bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/Image_5_2x_9580693a-f1ec-4902-863d-0c248df3bfb2.png?v=1614932896'
							/>
						</Div>
					</Div>
					<Text>
						In the normal arch, the foot has a defined arch with
						moderate flexibility.
					</Text>
					<Text>
						The most biomechanically efficient of them all, the foot
						rolls naturally under movement to absorb shock.
					</Text>
					<Text>
						For the normal arch, go for stability shoes with firm
						midsoles and straight to semi-curved lasts that have
						extra arch side-supports, and provide forefoot
						flexibility and rearfoot stability.
					</Text>
				</li>
				<li>
					<Text>HIGH ARCH</Text>
					<Div className='archImages'>
						<Div className='image-container'>
							<Div
								className='image1'
								bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/Image_3_2x_0e37f3a5-2da3-4cc0-a519-63a65804e059.png?v=1614932896'
							/>
						</Div>
						<Div className='image-container'>
							<Div
								className='image2'
								bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/Group_8336_2x_fb07835d-79ac-4900-9242-a0209c60dd6b.png?v=1614932894'
							/>
						</Div>
						<Div className='image-container'>
							<Div
								className='image3'
								bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/Image_6_2x_8d84521b-4cb1-492a-83bd-528d5fac4130.png?v=1614932896'
							/>
						</Div>
					</Div>
					<Text>
						In the high arch, the foot has an extremely well-defined
						arch than the usual and little flexibility.
					</Text>
					<Text>
						As the arch sits higher from the ground, there is less
						surface area for shock absorption, and excessive
						pressure is exerted upon the rearfoot and the forefoot
						during the movement. To correct the high arch, your best
						bet is cushioning footwear with a curved last and
						minimal rigidity to increase shock absorption.
					</Text>
				</li>
			</ul>
			<Text textAlign='center' textWeight='600' textSize='16px'>
				The best way to determine your feet shape, size, pressure, and
				movement are to get the feet scanned at least once every year.
			</Text>
		</Div>
	);
};
const GeneralInfoContent = () => {
	const data = [
		{
			name: 'Foot Length',
			left: 268,
			right: 269,
		},
		{
			name: 'Ball Width',
			left: 268,
			right: 269,
		},
		{
			name: 'Heel Width',
			left: 268,
			right: 269,
		},
		{
			name: 'Toe Height',
			left: 268,
			right: 269,
		},
		{
			name: 'Ball Girth',
			left: 268,
			right: 269,
		},
		{
			name: 'Ball Girth New',
			left: 268,
			right: 269,
		},
		{
			name: 'Instep Girth',
			left: 268,
			right: 269,
		},
		{
			name: 'Heel Girth',
			left: 268,
			right: 269,
		},
	];
	return (
		<Div className='marketplace-fitprofile-generalinfo'>
			<Div h='100%' d='flex' flexDir='column'>
				<Div w='100%' d='flex' justify='center'>
					<Div
						className='marketplace-fitprofile-generalinfo-image'
						bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/02_3D.png?v=1614941093'
					/>
				</Div>
				<Div className='marketplace-fitprofile-section-card-body-general'>
					<Div className='marketplace-fitprofile-section-card-body-general-row'>
						<Div className='marketplace-fitprofile-section-card-body-general-value' />
						<Div className='marketplace-fitprofile-section-card-body-general-data'>
							<Text>Left, mm</Text>
						</Div>
						<Div className='marketplace-fitprofile-section-card-body-general-data'>
							<Text>Right, mm</Text>
						</Div>
					</Div>
					{data.map((d, index) => (
						<Div
							key={index}
							className='marketplace-fitprofile-section-card-body-general-row'>
							<Div className='marketplace-fitprofile-section-card-body-general-value'>
								<Text>{d.name}</Text>
							</Div>
							<Div className='marketplace-fitprofile-section-card-body-general-data'>
								<Text>{d.left}</Text>
							</Div>
							<Div className='marketplace-fitprofile-section-card-body-general-data'>
								<Text>{d.right}</Text>
							</Div>
						</Div>
					))}
				</Div>
			</Div>
		</Div>
	);
};
const FeetTypeContent = () => {
	return (
		<Div className='marketplace-fitprofile-feettype'>
			<Div className='marketplace-fitprofile-feettype-section'>
				<Text>
					The foot type refers to the outline and arrangement of the
					lengths of the toes.
				</Text>
				<Text>
					According to ‘foot and toe ancestry’, the length of your
					phalanges can help in making quite an educated guess about
					where your ancestors might have originated from but these
					are generally not considered very accurate in today’s
					digital age where ancestry can be easily traced through DNA
					tests.
				</Text>
			</Div>
			<Div className='marketplace-fitprofile-feettype-section'>
				<Text>There are five major foot types:</Text>
				<Div className='feetImages'>
					<Div className='image-container'>
						<Div
							className='image'
							bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/Group_8339_2x_66d3214c-a857-4001-adc8-61b9ddb84f44.png?v=1614941616'
							h='10em'
							w='10em'
						/>
						<Text>EGYPTIAN</Text>
					</Div>
					<Div className='image-container'>
						<Div
							className='image'
							bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/Group_8348_2x_b9282603-4cf7-41a7-95f7-b6ec93885866.png?v=1614941812'
							h='10em'
							w='10em'
						/>
						<Text>ROMAN</Text>
					</Div>
					<Div className='image-container'>
						<Div
							className='image'
							bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/Group_8340_2x_b7832e46-6b65-44a2-b9f0-766f1e40a87b.png?v=1614941879'
							h='10em'
							w='10em'
						/>
						<Text>GERMAN</Text>
					</Div>
					<Div className='image-container'>
						<Div
							className='image'
							bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/Group_8344_2x_4eca5c38-b8f1-4931-b7c4-4b6b39b4564b.png?v=1614941909'
							h='10em'
							w='10em'
						/>
						<Text>GREEK</Text>
					</Div>
					<Div className='image-container'>
						<Div
							className='image'
							bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/Group_8347_2x_21503bb3-545e-4366-8801-85f0af76ed1f.png?v=1614941934'
							h='10em'
							w='10em'
						/>
						<Text>CELTIC</Text>
					</Div>
				</Div>
			</Div>
			<Div className='marketplace-fitprofile-feettype-section'>
				<ul>
					<li>
						<Text>
							<b>Egyptian:</b> In the Egyptian foot type, the toes
							are in descending order in size from the big toe to
							the little toe at an angle of 45 degrees. Since they
							are the most regular-shaped among all the foot
							types, most of the shoes are designed to cater to
							Egyptian foot type. Interestingly, they also have
							reduced chances of ingrown toenails than the other
							foot types.
						</Text>
					</li>
					<li>
						<Text>
							<b>Roman:</b> In the Roman foot type, the first
							three toes are the same in length while the other
							two are in descending order of size. One of the most
							common foot shapes across the world, Roman foot
							shape is a bit square and wide which leads to
							difficulty in wearing pointed footwear.
						</Text>
					</li>
					<li>
						<Text>
							<b>German:</b> In the German foot type, the big toe
							is the longest and the rest of the toes are of the
							same size which gives the foot a square shape. Wide
							shoes that do not taper at the front are the best
							bet for the German foot type.
						</Text>
					</li>
					<li>
						<Text>
							<b>Greek:</b> In the Greek foot type, the second toe
							is the longest, even longer than the big toe, and is
							considered to be an ideological geometric concept.
							This foot type is extremely prevalent in the ancient
							Greek statues. The elongated second toe is known to
							cause foot aches due to uneven distribution of the
							weight but also gives an athletic advantage to many.
						</Text>
					</li>
					<li>
						<Text>
							<b>Celtic:</b> The most complex and unusual of them
							all, the Celtic foot type consists of a large big
							toe, an elongated second toe like the Greek foot
							type, and the rest of the toes in descending size.
							Due to the complex foot type, standardised footwear
							are often ill-fitted and provide little comfort.
						</Text>
					</li>
				</ul>
			</Div>
		</Div>
	);
};

const Details = () => {
	const router = useRouter();
	const [state, setState] = useState({
		mode: null,
	});
	const pageStore = useSelector((state) => state.pageStore);
	const handleClick = (value) => {
		router.push({
			pathname: router.pathname,
			query: {
				type: value,
			},
		});
	};
	const handleContinueShopping = () => {
		router.push('/marketplace');
	};
	useEffect(() => {
		if (router.asPath.includes('/details?')) {
			if (router.query?.type) {
				switch (router.query.type) {
					case 'ARCH TYPE':
						setState({ ...state, mode: 'ARCH TYPE' });
						break;
					case 'GENERAL INFO':
						setState({ ...state, mode: 'GENERAL INFO' });
						break;
					case 'FEET TYPE':
						setState({ ...state, mode: 'FEET TYPE' });
						break;
					default:
						router.push({
							pathname: router.pathname,
							query: {
								type: 'GENERAL INFO',
							},
						});
						break;
				}
			}
		} else {
			router.push({
				pathname: router.pathname,
				query: {
					type: 'GENERAL INFO',
				},
			});
		}
	}, [router]);
	return (
		<ThemeProvider theme={theme}>
			<Div className='marketplace-fitprofile-container'>
				<Div className='marketplace-fitprofile-container-section'>
					<Div className='truklay-logo marketplace-fitprofile-image-size' />
					<Text className='marketplace-fitprofile-text'>
						Welcome Mr Karan Vadhwa
					</Text>
				</Div>
				<Div className='marketplace-fitprofile-container-section marketplace-fitprofile-container-section-middle'>
					<Div className='legaldocs-container'>
						<Div className='legaldocs-top-nav'>
							<CommonButton
								name='ARCH TYPE'
								handleClick={handleClick.bind(
									this,
									'ARCH TYPE',
								)}
								active={state.mode == 'ARCH TYPE'}
							/>
							<CommonButton
								name='GENERAL INFO'
								handleClick={handleClick.bind(
									this,
									'GENERAL INFO',
								)}
								active={state.mode == 'GENERAL INFO'}
							/>
							<CommonButton
								name='FEET TYPE'
								handleClick={handleClick.bind(
									this,
									'FEET TYPE',
								)}
								active={state.mode == 'FEET TYPE'}
							/>
						</Div>
						<Div className='legaldocs-section'>
							{state.mode == 'ARCH TYPE' && <ArchTypeContent />}
							{state.mode == 'GENERAL INFO' && (
								<GeneralInfoContent />
							)}
							{state.mode == 'FEET TYPE' && <FeetTypeContent />}
						</Div>
					</Div>
				</Div>
				<Div
					className={`marketplace-fitprofile-container-section marketplace-fitprofile-container-section-lower  ${
						pageStore.footerInView ? 'footer-inView' : ''
					}`}>
					<Div className='marketplace-fitprofile-container-section-button'>
						<Button
							p={{ x: '2em' }}
							onClick={handleContinueShopping.bind(this)}>
							CONTINUE SHOPPING
						</Button>
					</Div>
				</Div>
				<Div
					className={`marketplace-fitprofile-container-section-button-overlay ${
						pageStore.footerInView
							? 'marketplace-splashscreen-lower-section-button-overlay-with-footer'
							: ''
					}`}
				/>
			</Div>
		</ThemeProvider>
	);
};

export default Details;
