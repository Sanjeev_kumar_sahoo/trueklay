import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { ThemeProvider, Div, Text, Button } from 'atomize';
import { CarouselSlider } from '../../../Components/common/slider';
import Checkbox from '../../../Components/common/checkbox';
import { handleMarketplaceSplashScreen } from '../../../Stores/shopifystore';
import { setLoading } from '../../../Stores/pagestore';
import { useRouter } from 'next/router';

const theme = {
	colors: {
		brand: '#1a89f0',
		brandHover: '#ffc5c5',
	},
	shadows: {
		'card-shadow': '-2px 2px 8px 8px rgba(0, 0, 0, 0.04)',
		'splashscreen-card-shadow': '-2px 2px 8px 12px rgba(0, 0, 0, 0.02)',
		'splashscreen-card-shadow-title':
			'-2px 5px 10px 8px rgba(0, 0, 0, 0.08)',
	},
};

const SplashBoxContainer = (props) => {
	return (
		<Div className='marketplace-splashscreen-section-cardbox'>
			<Div className='marketplace-splashscreen-section-card'>
				{props.children}
			</Div>
		</Div>
	);
};
const SplashBoxContainerTitle = (props) => {
	return (
		<Div className='marketplace-splashscreen-section-card-title'>
			<Div shadow='splashscreen-card-shadow-title' w='fit-content'>
				<Text>{props.children}</Text>
			</Div>
		</Div>
	);
};
const SplashBoxContainerBody = (props) => {
	return (
		<Div
			className='marketplace-splashscreen-section-card-body'
			d='flex'
			justify='center'
			flexDir='column'
			shadow='splashscreen-card-shadow'>
			{props.children}
			{props.handleClick && (
				<Div className='marketplace-splashscreen-section-card-know-more'>
					<Text onClick={props.handleClick}>{`Know More >`}</Text>
				</Div>
			)}
		</Div>
	);
};

const Screen1 = () => {
	const router = useRouter();
	const handleClick = () => {
		router.push({
			pathname: '/marketplace/fitprofile/details',
			query: {
				type: 'ARCH TYPE',
			},
		});
	};
	return (
		<SplashBoxContainer>
			<SplashBoxContainerTitle>Arch Type</SplashBoxContainerTitle>
			<SplashBoxContainerBody handleClick={handleClick.bind(this)}>
				<Div className='marketplace-splashscreen-section-card-body-arch'>
					<Div>
						<Div
							bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/11.1_FlatArch.png?v=1614853796'
							bgSize='cover'
							bgPos='center'
							h='60px'
							w='120px'
						/>
						<Text>FLAT ARCH</Text>
					</Div>
					<Div>
						<Div
							bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/11.2_NeutralArch.png?v=1614853795'
							bgSize='cover'
							bgPos='center'
							h='60px'
							w='120px'
						/>
						<Text>NEUTRAL ARCH</Text>
					</Div>
					<Div>
						<Div
							bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/11.3_HighArch.png?v=1614853795'
							bgSize='cover'
							bgPos='center'
							h='60px'
							w='120px'
						/>
						<Text>HIGH ARCH</Text>
					</Div>
				</Div>
				<Div className='marketplace-splashscreen-section-card-body-content marketplace-splashscreen-section-not-mobile'>
					<Text p={{ y: '2em' }}>
						The arch of the foot refers to the area between the ball
						and the heel, along the bottom of the foot. Made up of
						three different arches - the medial longitudinal arch
						that runs from the end of the heel to the ball of the
						foot along the centre of the foot, the lateral
						longitudinal arch that runs along the outside edge of
						the foot, and the anterior transverse that runs from
						side to side, the arch of the foot provides support to
						the whole body during the day-to-day movement. Before
						getting any footwear, it is highly imperative to know
						your arch type as it helps in making an informed
						decision about the kind of footwear to choose that would
						support your foot arch.
					</Text>
					<Text>There are three kinds of arches:</Text>
				</Div>
			</SplashBoxContainerBody>
		</SplashBoxContainer>
	);
};
const Screen2 = () => {
	const router = useRouter();
	const handleClick = () => {
		router.push({
			pathname: '/marketplace/fitprofile/details',
			query: {
				type: 'GENERAL INFO',
			},
		});
	};
	const data = [
		{
			name: 'Foot Length',
			left: 268,
			right: 269,
		},
		{
			name: 'Ball Width',
			left: 268,
			right: 269,
		},
		{
			name: 'Heel Width',
			left: 268,
			right: 269,
		},
		{
			name: 'Toe Height',
			left: 268,
			right: 269,
		},
		{
			name: 'Ball Girth',
			left: 268,
			right: 269,
		},
		{
			name: 'Ball Girth New',
			left: 268,
			right: 269,
		},
		{
			name: 'Instep Girth',
			left: 268,
			right: 269,
		},
		{
			name: 'Heel Girth',
			left: 268,
			right: 269,
		},
	];
	return (
		<SplashBoxContainer>
			<SplashBoxContainerTitle>General Info</SplashBoxContainerTitle>
			<SplashBoxContainerBody handleClick={handleClick.bind(this)}>
				<Div h='fit-content' d='flex' flexDir='column'>
					<Div w='100%' d='flex' justify='center'>
						<Div
							bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/12_3D.png?v=1614853796'
							bgSize='cover'
							bgPos='center'
							h='10em'
							w='12em'
						/>
					</Div>
					<Div className='marketplace-splashscreen-section-card-body-general'>
						<Div className='marketplace-splashscreen-section-card-body-general-row'>
							<Div className='marketplace-splashscreen-section-card-body-general-value' />
							<Div className='marketplace-splashscreen-section-card-body-general-data'>
								<Text>Left, mm</Text>
							</Div>
							<Div className='marketplace-splashscreen-section-card-body-general-data'>
								<Text>Right, mm</Text>
							</Div>
						</Div>
						{data.map((d, index) => (
							<Div
								key={index}
								className='marketplace-splashscreen-section-card-body-general-row'>
								<Div className='marketplace-splashscreen-section-card-body-general-value'>
									<Text>{d.name}</Text>
								</Div>
								<Div className='marketplace-splashscreen-section-card-body-general-data'>
									<Text>{d.left}</Text>
								</Div>
								<Div className='marketplace-splashscreen-section-card-body-general-data'>
									<Text>{d.right}</Text>
								</Div>
							</Div>
						))}
					</Div>
				</Div>
			</SplashBoxContainerBody>
		</SplashBoxContainer>
	);
};
const Screen3 = () => {
	const router = useRouter();
	const handleClick = () => {
		router.push({
			pathname: '/marketplace/fitprofile/details',
			query: {
				type: 'FEET TYPE',
			},
		});
	};
	return (
		<SplashBoxContainer>
			<SplashBoxContainerTitle>Feet Type</SplashBoxContainerTitle>
			<SplashBoxContainerBody handleClick={handleClick.bind(this)}>
				<Div className='marketplace-splashscreen-section-card-body-feet'>
					<Div>
						<Div
							bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/13.1_FeetType.png?v=1614853795'
							bgSize='cover'
							bgPos='center'
							h='80px'
							w='110px'
						/>
						<Text>EGYPTIAN</Text>
					</Div>
					<Div>
						<Div
							bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/13.2_FeetType.png?v=1614853795'
							bgSize='cover'
							bgPos='center'
							h='80px'
							w='110px'
						/>
						<Text>ROMAN</Text>
					</Div>
					<Div>
						<Div
							bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/13.3_FeetType.png?v=1614853795'
							bgSize='cover'
							bgPos='center'
							h='80px'
							w='110px'
						/>
						<Text>GERMAN</Text>
					</Div>
					<Div>
						<Div
							bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/13.4_FeetType.png?v=1614853795'
							bgSize='cover'
							bgPos='center'
							h='80px'
							w='110px'
						/>
						<Text>GREEK</Text>
					</Div>
					<Div>
						<Div
							bgImg='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/13.5_FeetType.png?v=1614853795'
							bgSize='cover'
							bgPos='center'
							h='80px'
							w='110px'
						/>
						<Text>CELTIC</Text>
					</Div>
				</Div>
				<Div className='marketplace-splashscreen-section-card-body-content marketplace-splashscreen-section-not-mobile'>
					<Text p={{ y: '1em' }}>
						According to ‘foot and toe ancestry’, the length of your
						phalanges can help in making quite an educated guess
						about where your ancestors might have originated from
						but these are generally not considered very accurate in
						today’s digital age where ancestry can be easily traced
						through DNA tests. The foot type refers to the outline
						and arrangement of the lengths of the toes.
						<br />
						There are five major foot types:
					</Text>
					<ul style={{ margin: 0 }}>
						<li>
							<Text p={{ r: '4em' }}>
								<b>Egyptian:</b> In the Egyptian foot type, the
								toes are in descending order in size from the
								big toe to the little toe at an angle of 45
								degrees. Since they are the most regular-shaped
								among all the foot types, most of the shoes are
								designed to cater to Egyptian foot type.
								Interestingly, they also have reduced chances of
								ingrown toenails than the other foot types.
							</Text>
						</li>
					</ul>
				</Div>
			</SplashBoxContainerBody>
		</SplashBoxContainer>
	);
};

const FitProfile = () => {
	const router = useRouter();
	const dispatch = useDispatch();
	const [checked, setChecked] = useState(false);
	const handleContinueShopping = () => {
		dispatch(
			handleMarketplaceSplashScreen({
				actionType: 'CLOSE',
			}),
		);
		router.push('/marketplace');
	};
	useEffect(() => {
		dispatch(setLoading({ loading: true }));
	}, []);
	return (
		<ThemeProvider theme={theme}>
			<Div className='marketplace-splashscreen-container'>
				<Div className='marketplace-splashscreen-upper'>
					<Div className='truklay-logo marketplace-splashscreen-image-size' />
					<Text className='marketplace-splashscreen-text'>
						Welcome Mr Karan Vadhwa
					</Text>
				</Div>
				<Div className='marketplace-splashscreen-middle'>
					<Div className='marketplace-splashscreen-section'>
						<CarouselSlider content={[Screen1, Screen2, Screen3]} />
					</Div>
				</Div>
				<Div className='marketplace-splashscreen-lower'>
					<Div className='marketplace-splashscreen-lower-section'>
						<Checkbox
							checked={checked}
							handleChange={() => {
								setChecked(!checked);
							}}
							label={
								<Text>
									Don’t wish to see this screen again.
								</Text>
							}
						/>
					</Div>
					<Div className='marketplace-splashscreen-lower-section marketplace-splashscreen-lower-section-button '>
						<Button onClick={handleContinueShopping.bind(this)}>
							Continue Shopping
						</Button>
					</Div>
					<Div className='marketplace-splashscreen-lower-section-button-overlay'/>
				</Div>
				<Div className='marketplace-splashscreen-close-button'>
					<Text>X</Text>
				</Div>
			</Div>
		</ThemeProvider>
	);
};

export default FitProfile;
