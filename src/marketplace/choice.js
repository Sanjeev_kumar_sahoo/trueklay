import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Div, Button } from 'atomize';
import { useRouter } from 'next/router';

const MarketPlaceChoice = () => {
	const router = useRouter();
	const shopifyStore = useSelector((state) => state.shopifyStore);
	const handleClick = (type) => {
		router.push({
			pathname: '/marketplace',
			query: {
				type,
			},
		});
	};
	useEffect(() => {
		if (shopifyStore?.client?.keepMarketplaceSplashScreen) {
			router.push('/marketplace/fitprofile');
		}
	}, []);
	return (
		<Div className='marketplace-choice-container'>
			<Div
				className='marketplace-choice-container-section'
				bgImg='https://images.unsplash.com/photo-1577106436954-eb29e215beff?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80'
				bgSize='cover'
				bgPos='center'
				w='100%'
			/>
			<Div className='marketplace-choice-container-section marketplace-choices'>
				<Div className='marketplace-choices-section'>
					<Div
						onClick={handleClick.bind(this, 'SHOE')}
						className='marketplace-choices-section-inner'
						flexGrow='1'
						pos='relative'
						bgImg='https://images.unsplash.com/photo-1600269452121-4f2416e55c28?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=701&q=80'
						bgSize='cover'
						bgPos='center'>
						<Button>SHOP FOOTWARE</Button>
					</Div>
				</Div>
				<Div className='marketplace-choices-section'>
					<Div
						onClick={handleClick.bind(this, 'INSOLE')}
						className='marketplace-choices-section-inner'
						flexGrow='1'
						pos='relative'
						bgImg='https://images.unsplash.com/photo-1555100343-2c5faae4dcfe?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80'
						bgSize='cover'
						bgPos='center'>
						<Button>SHOP INSOLES</Button>
					</Div>
				</Div>
			</Div>
		</Div>
	);
};

export default MarketPlaceChoice;
