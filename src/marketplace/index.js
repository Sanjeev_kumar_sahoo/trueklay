import React, { useState, useEffect } from 'react';
import { ThemeProvider, Div, Text, Button, Notification } from 'atomize';
import { useSelector, useDispatch } from 'react-redux';

import {
	loadShopifyStore,
	toggleFilterValue,
	addToCart,
	toggleCart,
	toggleFilter,
	handleProduct,
	handleMarketplaceSplashScreen,
	handlePagination,
} from '../../Stores/shopifystore';
import { setTeknofeetClientID } from '../../Stores/teknofeetstore';
import { setLoading } from '../../Stores/pagestore';

import CardBox from '../../Components/marketplace/cardbox';
import { FilterCard } from '../../Components/marketplace/filtercard';
import PaginationPanel from '../../Components/common/paginationpanel';

import { useRouter } from 'next/router';
import { handleUserBrowserData } from '../../helpers';
import { deviceType } from '../../helpers/divelements';

const theme = {
	grid: {
		colCount: 12,
		gutterWidth: '20px',
	},
	colors: {
		brand: '#1a89f0',
		brandHover: '#ffc5c5',
	},
	shadows: {
		'card-shadow': '-2px 2px 8px 8px rgba(0, 0, 0, 0.04)',
		'splashscreen-card-shadow': '-2px 2px 8px 12px rgba(0, 0, 0, 0.02)',
		'splashscreen-card-shadow-title':
			'-2px 5px 10px 8px rgba(0, 0, 0, 0.08)',
	},
};

export default function MarketPlace() {
	const shopifyStore = useSelector((state) => state.shopifyStore);
	const pageStore = useSelector((state) => state.pageStore);
	const dispatch = useDispatch();
	const router = useRouter();
	const onFilterClick = (title, name) => {
		dispatch(toggleFilterValue({ title, name }));
	};
	const handleCardClick = (id) => {
		router.push({
			pathname: '/marketplace/product',
			query: { pid: id },
		});
	};
	useEffect(() => {
		dispatch(loadShopifyStore());
		// if (shopifyStore?.client?.keepMarketplaceSplashScreen) {
		// 	router.push('/marketplace/fitprofile');
		// }
		let teknofeetLoggedInUser = handleUserBrowserData('GETUSER');
		if (teknofeetLoggedInUser) {
			const {
				value,
				password,
				valueType,
				status,
			} = teknofeetLoggedInUser;
			dispatch(setTeknofeetClientID({ [valueType]: value }));
		}
	}, []);
	return (
		<ThemeProvider theme={theme}>
			<Div d='flex' flexDir='column' height='100%'>
				<Div className='marketplace-section'>
					<Div className='marketplace-gallery-image' />
				</Div>
				<Div className='marketplace-section'>
					<Div className='marketplace-filter-userId'>
						<Div d='flex' align='center'>
							<Div className='filter-icon marketplace-filter-icon' />
							<Text className='filter-text'>FILTER</Text>
						</Div>
						<Div d='flex' align='center'>
							<Text className='user-id-text'>
								MR. KARAN VADHWA
							</Text>
							<Div className='user-name-id-separator' />
							<Text className='user-id-text'>UFID 00718293</Text>
						</Div>
					</Div>
				</Div>
				<Div className='horizontal-separator' />
				<Div className='marketplace-section' d='flex' flexGrow='1'>
					<Div className='marketplace-filter'>
						<Div className='marketplace-filter-cards'>
							{shopifyStore.filters.map((data, index) => (
								<FilterCard
									data={data}
									onFilterClick={onFilterClick.bind(
										this,
										data.title,
									)}
									key={`filterCard${index}`}
								/>
							))}
						</Div>
						<Button
							w='100%'
							h='40px'
							rounded='none'
							bg='brand'
							border='1px solid'
							borderColor='brand'
							hoverBg='white'
							hoverTextColor='brand'>
							RESET ALL FILTERS
						</Button>
					</Div>
					{/* <Div className='vertical-separator' /> */}
					<Div className='marketplace-menu'>
						<ul>
							{shopifyStore.filteredProducts?.slice(
									shopifyStore.pagination.index *
										shopifyStore.pagination.maxContentPerPage,
									(shopifyStore.pagination.index + 1) *
										shopifyStore.pagination.maxContentPerPage,
								).map((product, index) => (
									<li key={`card_ID-${product.id}`}>
										<CardBox
											handleProductClick={handleCardClick.bind(
												this,
												product.id,
											)}
											addToCart={() => {
												const isAdded =
													shopifyStore.cart.filter(
														(f) =>
															f.id == product.id,
													).length != 0;
												dispatch(
													addToCart({
														id: product.id,
														cartStat:
															deviceType() !==
															'mobile',
														added: isAdded,
													}),
												);
											}}
											added={
												shopifyStore.cart.filter(
													(f) => f.id == product.id,
												).length != 0
											}
											toggleWishlist={() => {
												dispatch(
													handleProduct({
														id: product.id,
														actionType: product.addedToWishlist
															? 'REMOVE_FROM_WISHLIST'
															: 'ADD_TO_WISHLIST',
													}),
												);
											}}
											product={product}
										/>
									</li>
								))}
							{shopifyStore.filteredProducts?.map(
								(dummy, index) => (
									<li
										className='marketplace-dummy-card'
										key={`dummy_card_ID-${index}`}
									/>
								),
							)}
						</ul>
						<Div className='paginationPanel'>
							<PaginationPanel
								totalPage={
									shopifyStore.allProducts.length /
									shopifyStore.pagination.maxContentPerPage
								}
								handlePageClick={(index) => {
									dispatch(
										handlePagination({
											actionType: 'CHANGEPAGE',
											index,
										}),
									);
								}}
							/>
						</Div>
					</Div>
				</Div>
				<Div className='marketplace-section'>
					<Div
						className={
							pageStore.footerInView
								? 'marketplace-bottom-navbar'
								: 'marketplace-bottom-navbar marketplace-bottom-navbar-is-fixed'
						}>
						<Button
							className='marketplace-bottom-navbar-secondary-button'
							onClick={() => {
								dispatch(toggleFilter());
							}}>
							FILTER
						</Button>
						<Button
							className='marketplace-bottom-navbar-primary-button'
							onClick={() => {
								dispatch(toggleCart());
							}}>
							BAG
						</Button>
					</Div>
					<Div
						className={
							pageStore.footerInView
								? 'marketplace-bottom-navbar-overlay marketplace-bottom-navbar-overlay-disable'
								: 'marketplace-bottom-navbar-overlay'
						}
					/>
				</Div>
				<Div className='marketplace-section'>
					<Div w='100%' p={{ x: '40px', y: '20px' }}>
						<Div d='flex' justify='space-between' w='100%'>
							<Div w='30%' border='1px solid' borderColor='black'>
								ajshkdhgs
							</Div>
							<Div w='30%' border='1px solid' borderColor='black'>
								ajshkdhgs
							</Div>
							<Div w='30%' border='1px solid' borderColor='black'>
								ajshkdhgs
							</Div>
						</Div>
					</Div>
				</Div>
			</Div>
		</ThemeProvider>
	);
}
