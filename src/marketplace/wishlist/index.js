import React, { useEffect } from 'react';
import { ThemeProvider, Icon, Button, Div, Text } from 'atomize';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import {
	handleProduct,
	addToCart,
	loadShopifyStore,
} from '../../../Stores/shopifystore';

import WishlistCardBox from '../../../Components/marketplace/wishlist/wishlistcardbox';

const theme = {
	grid: {
		colCount: 12,
		gutterWidth: '20px',
	},
	colors: {
		brand: '#1a89f0',
		brandHover: '#ffc5c5',
	},
	shadows: {
		'card-shadow': '-2px 2px 8px 8px rgba(0, 0, 0, 0.04)',
	},
};

export default function Wishlist() {
	const router = useRouter();
	const shopifyStore = useSelector((state) => state.shopifyStore);
	const dispatch = useDispatch();
	const handleCardClick = (id) => {
		router.push({
			pathname: '/marketplace/product',
			query: { pid: id },
		});
	};
	useEffect(() => {
		dispatch(loadShopifyStore());
	}, []);
	return (
		<ThemeProvider theme={theme}>
			<Div className='wishlist-container'>
				{shopifyStore.filteredProducts.filter(
					(product) => product.addedToWishlist,
				).length > 0 ? (
					<>
						<Div className='marketplace-section wishlist-page-header'>
							<Div
								d='flex'
								align='center'
								w='fit-content'
								h='60px'
								pos='absolute'>
								<Button
									rounded='circle'
									border='1px solid'
									borderColor='brand'
									bg='transparent'
									w='fit-content'
									h='fit-content'
									m='0px'
									p='2px'>
									<Icon
										name='LeftArrow'
										size='20px'
										color='brand'
									/>
								</Button>
								<Text
									textSize='16px'
									m={{ l: '1em' }}
									w='max-content'>
									Back to results
								</Text>
							</Div>
							<Div
								h='60px'
								flexGrow='1'
								d='flex'
								justify='center'
								align='center'>
								<Text
									textSize='24px'
									textWeight='600'
									m={{ l: '1em' }}
									w='max-content'>
									My Wishlist
								</Text>
							</Div>
						</Div>
						<Div className='marketplace-section'>
							<Div className='marketplace-menu'>
								<ul>
									{shopifyStore.filteredProducts
										.filter(
											(product) =>
												product.addedToWishlist,
										)
										.map((product) => (
											<li
												key={`wishlistCardKey-${product.id}`}>
												<WishlistCardBox
													product={product}
													handleProductClick={handleCardClick.bind(
														this,
														product.id,
													)}
													removeFromWishList={() => {
														dispatch(
															handleProduct({
																id: product.id,
																actionType:
																	'REMOVE_FROM_WISHLIST',
															}),
														);
													}}
													addToCart={() => {
														dispatch(
															handleProduct({
																id: product.id,
																actionType:
																	'REMOVE_FROM_WISHLIST',
															}),
														);
														dispatch(
															addToCart({
																id: product.id,
															}),
														);
													}}
												/>
											</li>
										))}
									{shopifyStore.filteredProducts
										.filter(
											(product) =>
												product.addedToWishlist,
										)
										.map((product, index) => (
											<li
												className='marketplace-dummy-card'
												key={
													'marketplace-dummy-card-' +
													index
												}
											/>
										))}
								</ul>
							</Div>
						</Div>
						<Div flexGrow='1' />
						{/* <Div flexGrow='0.5' /> */}
					</>
				) : (
					<>
						<Div className='marketplace-section wishlist-page-header'>
							<Div
								h='60px'
								flexGrow='1'
								d='flex'
								justify='center'
								align='center'>
								<Text
									textSize='24px'
									textWeight='600'
									w='max-content'>
									My Wishlist
								</Text>
							</Div>
						</Div>
						<Div flexGrow='0.1' />
						<Div
							className='marketplace-section'
							m='0 auto'
							d='flex'
							justify='center'
							align='center'
							flexDir='column'>
							<Div
								className='empty-wishlist-icon wishlist-empty-icon'
								m={{ b: '1em' }}></Div>
							<Text
								textColor='brand'
								textWeight='600'
								textSize='18px'
								m={{ b: '1em' }}>
								Your wishlist is empty !
							</Text>
							<Text>
								Add items that you like to your wishlist.
							</Text>
						</Div>
						<Div className='marketplace-section'>
							<Div d='flex' justify='center'>
								<Link href='/marketplace'>
									<a>
										<Button
											rounded='circle'
											border='1px solid'
											borderColor='brand'
											bg='brand'
											w='fit-content'
											h='40px'>
											<Text
												textWeight='600'
												style={{
													letterSpacing: '1px',
												}}>
												CONTINUE SHOPPING
											</Text>
										</Button>
									</a>
								</Link>
							</Div>
						</Div>
						<Div flexGrow='0.1' />
					</>
				)}
			</Div>
		</ThemeProvider>
	);
}
