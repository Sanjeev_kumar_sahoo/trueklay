import React, { useRef, useState, useEffect } from 'react';
import { ThemeProvider, Icon, Button, Div, Text, Input } from 'atomize';
import Link from 'next/link';
import { debounce } from 'lodash';
import Popup from '../../../Components/common/popup';
import {
	BasicDropdown,
	MultipleRadioChoice,
} from '../../../Components/common/multiplechoice';
import Datepicker from '../../../Components/common/customdatepicker';

const theme = {
	grid: {
		colCount: 12,
		gutterWidth: '20px',
	},
	colors: {
		brand: '#1a89f0',
	},
	shadows: {
		'card-shadow': '-2px 2px 8px 8px rgba(0, 0, 0, 0.04)',
	},
};

const inputType = [
	{
		labels: ['Full Name', 'Mobile No.', 'address_value'],
		type: 'text',
	},
	{
		labels: [],
		type: 'textarea',
	},
	{
		labels: [],
		type: 'number',
	},
	{
		labels: ['Date of birth'],
		type: 'date',
	},
	{
		labels: ['Email Id'],
		type: 'email',
	},
	{
		labels: ['Password'],
		type: 'password',
	},
	{
		labels: ['Gender', 'address_name'],
		type: 'choice',
	},
];

const defaultInputDropdownOption = {
	gender: [
		'Male',
		'Female',
		// 'Transgender',
		// `Do not prefer to answer`
	],
	address_name: ['Home', 'Office', 'Custom'],
};

const specialPopupList = ['Mobile No.', 'Password'];
const specialPopupPencil = ['Mobile No.'];
const specialPopupNextLine = ['Password'];

const EditInputMode = ({ name, value, handleChange, inputClassName }) => {
	switch (inputType.filter((type) => type.labels.includes(name))[0].type) {
		case 'text':
			return (
				<Input
					w='100%'
					className={inputClassName ? inputClassName : ''}
					value={value}
					onChange={(e) => handleChange(e.target.value)}
				/>
			);
		case 'textarea':
			return (
				<Input
					w='100%'
					value={value}
					onChange={(e) => handleChange(e.target.value)}
				/>
			);
		case 'number':
			return (
				<Input
					type='number'
					w='100%'
					value={value}
					onChange={(e) => handleChange(e.target.value)}
				/>
			);
		case 'date':
			return (
				<Datepicker
					classes={inputClassName ? inputClassName : ''}
					value={value}
				/>
			);
		case 'email':
			return (
				<Input
					w='100%'
					value={value}
					onChange={(e) => handleChange(e.target.value)}
				/>
			);
		case 'password':
			return (
				<Input
					type='password'
					w='100%'
					value={value}
					onChange={(e) => handleChange(e.target.value)}
				/>
			);
		case 'choice':
			let TEMP_OPTIONS_CHOICE =
				defaultInputDropdownOption[String(name).toLowerCase()];
			// console.log(TEMP_OPTIONS_CHOICE);
			return (
				<MultipleRadioChoice
					classes={inputClassName ? inputClassName : ''}
					menuList={TEMP_OPTIONS_CHOICE}
					value={value}
					handleChange={(e) => {
						handleChange(e);
					}}
				/>
			);
		case 'dropdown':
			let TEMP_OPTIONS =
				defaultInputDropdownOption[String(name).toLowerCase()];
			// console.log(TEMP_OPTIONS);
			return (
				<BasicDropdown
					classes={inputClassName ? inputClassName : ''}
					menuList={TEMP_OPTIONS}
					value={value}
					handleChange={(e) => {
						handleChange(e);
					}}
				/>
			);
		default:
			return (
				<Input
					w='100%'
					value={value}
					onChange={(e) => handleChange(e.target.value)}
				/>
			);
	}
};

const ProfileSettingCard = ({
	profileDetailsRef,
	loginDetailsRef,
	addressDetailsRef,
}) => {
	const [mode, setMode] = useState('profile');
	const [modePos, setModePos] = useState(null);
	const executeScroll = (ref, mode) => {
		window.scrollTo({
			top: ref.current.offsetTop - 100,
			behavior: 'smooth',
		});
		setMode(mode);
	};
	const handleModeChange = (mode) => {
		switch (mode) {
			case 'profile':
				executeScroll(profileDetailsRef, mode);
				break;
			case 'login':
				executeScroll(loginDetailsRef, mode);
				break;
			case 'saved_addresses':
				executeScroll(addressDetailsRef, mode);
				break;
			default:
				console.log('hehe');
				break;
		}
	};
	const handleModeChangeOnScroll = debounce(() => {
		let p = window.scrollY;
		let t =
			document.documentElement.scrollHeight -
			document.documentElement.clientHeight;
		if (p < modePos[0][1] && p != t) {
			setMode('profile');
		} else if (p >= modePos[1][0] && p < modePos[1][1] && p != t) {
			setMode('login');
		} else {
			setMode('saved_addresses');
		}
	}, 100);
	const updateModePos = () => {
		let pos = [
			profileDetailsRef.current.offsetTop,
			loginDetailsRef.current.offsetTop,
			addressDetailsRef.current.offsetTop,
		];
		let range = [
			[0, (pos[0] + pos[1]) / 2],
			[(pos[0] + pos[1]) / 2, (pos[1] + pos[2]) / 2],
			[(pos[1] + pos[2]) / 2, 'eof'],
		];
		setModePos(range);
	};
	useEffect(() => {
		updateModePos();
	}, []);
	useEffect(() => {
		if (modePos != null) {
			window.scrollTo({
				top: 0,
				behavior: 'smooth',
			});
			window.addEventListener(
				'scroll',
				handleModeChangeOnScroll.bind(this),
			);
		}
		return () => {
			window.removeEventListener(
				'scroll',
				handleModeChangeOnScroll.bind(this),
			);
		};
	}, [modePos]);
	return (
		<Div className='profile-setting'>
			<Text className='profile-setting-user-name'>Karan Vadhwa</Text>
			<Text className='profile-setting-user-id' m={{ b: '1en' }}>
				UFID 128376281
			</Text>
			<Button
				onClick={() => {
					handleModeChange('profile');
				}}
				className={mode == 'profile' && 'profile-button-active'}
				prefix={<Icon name='User' size='24px' m={{ r: '0.5rem' }} />}>
				Profile
			</Button>
			<Button
				onClick={() => {
					handleModeChange('login');
				}}
				className={mode == 'login' && 'profile-button-active'}
				prefix={<Icon name='Logout' size='24px' m={{ r: '0.5rem' }} />}>
				Login
			</Button>
			<Button
				onClick={() => {
					handleModeChange('saved_addresses');
				}}
				className={mode == 'saved_addresses' && 'profile-button-active'}
				prefix={
					<Icon name='Location' size='24px' m={{ r: '0.5rem' }} />
				}>
				Saved Addresses
			</Button>
		</Div>
	);
};

const ProfileSectionCard = (props) => {
	return <Div className='profile-section'>{props.children}</Div>;
};

const ProfileDetails = (props) => {
	const { profileDetailsRef, updateUserData, data } = props;
	const [profileData, setProfileData] = useState(data['profileData']);
	const [editMode, setEditMode] = useState(false);
	const handleProfileData = (type, value) => {
		setProfileData(
			profileData.map((d) => (d.name == type ? { ...d, value } : d)),
		);
	};
	const handleUpdate = () => {
		updateUserData(profileData);
		setEditMode(!editMode);
	};
	const handleDiscard = () => {
		setProfileData(data['profileData']);
		setEditMode(!editMode);
	};
	return (
		<ProfileSectionCard>
			<Div ref={profileDetailsRef} />
			<Div d='flex' w='100%' justify='space-between'>
				<Text className='profile-section-title'>PROFILE DETAILS</Text>
				<Div flexGrow='1' />
				<Button
					onClick={setEditMode.bind(this, !editMode)}
					className={
						editMode
							? 'profile-section-edit-button-active'
							: 'profile-section-edit-button'
					}
					prefix={
						<Icon
							name='Edit'
							color='brand'
							size='20px'
							m={{ r: '0.5em' }}
						/>
					}>
					EDIT
				</Button>
			</Div>
			<Div flexGrow='1' />
			<Div className='profile-section-content'>
				{profileData?.map((data, index) => (
					<Div className='profile-section-content-row' key={index}>
						<Text className='profile-section-content-left'>
							{data.name}
						</Text>
						<Div className='profile-section-content-right'>
							<Text>{data.value}</Text>
						</Div>
					</Div>
				))}
			</Div>
			<Popup
				popupType='modal-popup-with-backbutton-mobile modal-popup-individual-padding modal-popup-bottom-button'
				open={editMode}
				onClose={handleDiscard.bind(this)}
				popupHeader={
					<Text className='profile-section-title'>
						PROFILE DETAILS
					</Text>
				}
				popupContent={
					<Div className='profileDetails-section'>
						{profileData?.map((data, index) => (
							<Div
								className='profileDetails-section-content'
								key={index}>
								<Text>{data.name}</Text>
								<Div>
									{EditInputMode({
										inputClassName: 'profile-button',
										name: data.name,
										value: data.value,
										handleChange: handleProfileData.bind(
											this,
											data.name,
										),
									})}
								</Div>
							</Div>
						))}
						<Div flexGrow='1' />
					</Div>
				}
				popupFooter={
					<>
						<Div flexGrow='1' />
						<Div d='flex' justify='flex-end' m={{ t: '1em' }}>
							<Button
								onClick={handleDiscard.bind(this)}
								className='profile-section-edit-button-active equal-padding modal-footer-secondary-button'>
								CANCEL
							</Button>
							<Button
								onClick={handleUpdate.bind(this)}
								className='profile-section-edit-button equal-padding modal-footer-primary-button'>
								UPDATE
							</Button>
						</Div>
					</>
				}
			/>
		</ProfileSectionCard>
	);
};
const LoginDetails = ({ loginDetailsRef, updateUserData, data }) => {
	const [loginData, setLoginData] = useState(data['loginData']);
	const [editMode, setEditMode] = useState(false);
	const [editType, setEditType] = useState(false);
	const handleUpdate = () => {
		updateUserData(loginData);
		setEditMode(!editMode);
	};
	const handleDiscard = () => {
		setLoginData(data['loginData']);
		setEditMode(!editMode);
	};
	return (
		<ProfileSectionCard>
			<Div ref={loginDetailsRef} />
			<Div d='flex' w='100%' justify='space-between'>
				<Text className='profile-section-title'>LOGIN DETAILS</Text>
				{/* <Div flexGrow='1' />
				<Button
					onClick={setEditMode.bind(this, !editMode)}
					className={
						editMode
							? 'profile-section-edit-button-active'
							: 'profile-section-edit-button'
					}
					prefix={
						<Icon
							name='Edit'
							color='brand'
							size='20px'
							m={{ r: '0.5em' }}
						/>
					}>
					EDIT
				</Button> */}
			</Div>
			<Div flexGrow='1' />
			<Div className='profile-section-content'>
				{loginData.map((data, index) => (
					<Div className='profile-section-content-row' key={index}>
						<Text className='profile-section-content-left'>
							{data.name}
						</Text>
						<Div className='profile-section-content-right'>
							<Div
								d='flex'
								justify='space-between'
								align='center'
								className={
									specialPopupNextLine.includes(data.name)
										? 'profile-section-content-right-nextline-format'
										: undefined
								}>
								<Text className='text-wrap-ellipse'>
									{data.value}
								</Text>
								{specialPopupList.includes(data.name) && (
									<>
										{specialPopupPencil.includes(
											data.name,
										) ? (
											<>
												<Text
													className='not-big-tablet'
													minW='max-content'
													onClick={() => {
														setEditType(data.name);
														setEditMode(true);
													}}
													textColor='brand'
													style={{
														cursor: 'pointer',
													}}>
													{`Change ${data.name} ?`}
												</Text>
												<Icon
													className='big-tablet'
													name='Edit'
													color='brand'
													size='20px'
													// m={{ r: '0.5em' }}
													onClick={() => {
														setEditType(data.name);
														setEditMode(true);
													}}
												/>
											</>
										) : (
											<Text
												minW='max-content'
												onClick={() => {
													setEditType(data.name);
													setEditMode(true);
												}}
												textColor='brand'
												style={{
													cursor: 'pointer',
												}}>
												{`Change ${data.name} ?`}
											</Text>
										)}
									</>
								)}
							</Div>
						</Div>
					</Div>
				))}
			</Div>
			{/* CHange Mobile Number */}
			{/* Change Password */}
			<Popup
				popupType={`modal-popup-with-backbutton-mobile modal-popup-individual-padding modal-popup-bottom-button`}
				onClose={handleDiscard.bind(this)}
				open={editMode}
				popupHeader={
					specialPopupList.includes(editType) ? (
						<Text className='profile-section-title'>
							CHANGE {`${editType}`}
						</Text>
					) : (
						<Text className='profile-section-title'>
							LOGIN DETAILS
						</Text>
					)
				}
				popupContent={
					<>
						{editType == 'Password' && (
							<Div className='profile-section-content'>
								<Div
									m={{ b: '0.5em', t: '1em' }}
									textColor='#3e3e3e'>
									<Text m={{ b: '0.4em' }} textWeight='600'>
										Old Password
									</Text>
									<Input
										placeholder='Old Password'
										rounded='circle'
									/>
								</Div>
								<Div m={{ y: '0.5em' }} textColor='#3e3e3e'>
									<Text m={{ b: '0.4em' }} textWeight='600'>
										New Password
									</Text>
									<Input
										placeholder='New Password'
										rounded='circle'
									/>
								</Div>
								<Div m={{ y: '0.5em' }} textColor='#3e3e3e'>
									<Text m={{ b: '0.4em' }} textWeight='600'>
										Confirm New Password
									</Text>
									<Input
										placeholder='Confirm New Password'
										rounded='circle'
									/>
								</Div>
							</Div>
						)}
					</>
				}
				popupFooter={
					<>
						<Div flexGrow='1' />
						<Div d='flex' justify='flex-end' m={{ t: '1em' }}>
							<Button
								className={
									'profile-section-edit-button-active equal-padding modal-footer-secondary-button'
								}>
								CANCEL
							</Button>
							<Button
								onClick={handleUpdate.bind(this)}
								className='profile-section-edit-button equal-padding modal-footer-primary-button'>
								UPDATE
							</Button>
						</Div>
					</>
				}
			/>
		</ProfileSectionCard>
	);
};
const AddressDetails = ({ addressDetailsRef, updateUserData, data }) => {
	const [addressData, setAddressData] = useState(
		data['addressData'].map((e) => ({ ...e, editMode: false })),
	);
	const [currentAddress, setCurrentAddress] = useState(null);
	const [addMode, setAddMode] = useState(false);
	const [editMode, setEditMode] = useState(false);
	const [addressNameOthers, setAddressNameOthers] = useState(false);
	const handleAddressChange = (index, type, value) => {
		let currAddress = {
			...currentAddress,
			data: {
				...currentAddress.data,
				[type]: value,
			},
		};
		setCurrentAddress({
			...currAddress,
			data: {
				...currAddress.data,
				name: defaultInputDropdownOption['address_name'].includes(
					currAddress.data.name,
				)
					? currAddress.data.name
					: 'Custom',
				name2: currAddress.data?.name2 ? currAddress.data.name2 : '',
			},
		});
	};
	const handleDeleteAddress = (index) => {
		setAddressData(addressData.filter((d, i) => i != index));
	};
	const toggleAddressEditMode = (index, mode) => {
		let temp = addressData.map((e, i) =>
			i == index ? { ...e, editMode: !e.editMode } : e,
		);
		if (mode == 'add-update') {
			temp[currentAddress.index] = {
				...currentAddress.data,
				name:
					currentAddress.data.name == 'Custom'
						? currentAddress.data.name2
						: currentAddress.data.name,
				editMode: false,
			};
		}
		if (addMode) {
			if (mode == 'discard') {
				temp = temp.slice(0, -1);
			}
			setAddMode(!addMode);
		}
		setEditMode(!editMode);
		setCurrentAddress(
			!editMode && {
				index,
				data: {
					...addressData[index],
					name: defaultInputDropdownOption['address_name'].includes(
						addressData[index].name,
					)
						? addressData[index].name
						: 'Custom',
					name2: defaultInputDropdownOption['address_name'].includes(
						addressData[index].name,
					)
						? ''
						: addressData[index].name,
				},
			},
		);
		setAddressData(temp);
	};
	const handleAddAddress = () => {
		setAddressData([
			...addressData,
			{ name: '', value: '', editMode: true },
		]);
		setCurrentAddress({
			index: addressData.length,
			data: { name: 'Home', name2: '', value: '', editMode: true },
		});
		setAddMode(!addMode);
		setEditMode(!editMode);
	};
	useEffect(() => {
		if (!editMode) {
			setCurrentAddress(null);
		}
	}, [editMode]);
	return (
		<ProfileSectionCard>
			<Div ref={addressDetailsRef} />
			<Div d='flex' w='100%' justify='space-between'>
				<Text className='profile-section-title'>SAVED ADDRESSES</Text>
				{!editMode && !addMode && (
					<Button
						onClick={handleAddAddress.bind(this)}
						className='profile-section-add-button'
						p={{ r: '2em', l: '1em' }}
						prefix={
							<Icon
								name='Plus'
								color='brand'
								size='20px'
								m={{ r: '0.5em' }}
							/>
						}>
						ADD
					</Button>
				)}
			</Div>
			<Div flexGrow='1' />
			{addressData.length > 0 ? (
				addressData
					.filter((data) => data.name)
					.map((data, index) => (
						<Div className='profile-section-content' key={index}>
							<Div className='profile-section-address'>
								<Div className='profile-section-address-left'>
									<Div>
										<Text className='profile-section-address-name'>
											{data.name}
										</Text>
										<Text className='profile-section-address-value'>
											{/* {data.value} */}
											{`${data.value2?.address}, ${data.value2?.city}, ${data.value2?.state}-${data.value2?.pincode}.`}
										</Text>
									</Div>
								</Div>
								<Div className='profile-section-address-right'>
									<Div w='max-content' h='100%'>
										<Icon
											onClick={handleDeleteAddress.bind(
												this,
												index,
											)}
											className='profile-section-address-icon'
											name='Delete'
											color='#3e3e3e'
											size='24px'
										/>
										<Icon
											onClick={toggleAddressEditMode.bind(
												this,
												index,
											)}
											className='profile-section-address-icon'
											name='Edit'
											color='#3e3e3e'
											size='24px'
										/>
									</Div>
								</Div>
							</Div>
						</Div>
					))
			) : (
				<Text>Add new address</Text>
			)}
			<Popup
				popupType='modal-popup-with-backbutton-mobile modal-popup-individual-padding modal-popup-bottom-button'
				open={editMode}
				onClose={toggleAddressEditMode.bind(
					this,
					currentAddress?.index,
					'discard',
				)}
				popupHeader={
					currentAddress && addMode ? (
						<Text className='profile-section-title'>
							ADD ADDRESS
						</Text>
					) : (
						<Text className='profile-section-title'>
							EDIT ADDRESS
						</Text>
					)
				}
				popupContent={
					currentAddress && (
						<Div
							className='profile-section-content'
							key={currentAddress.index}>
							<Div className='profile-section-address'>
								<Div className='profile-section-address-left'>
									<Div w='100%' className='addressDetails'>
										<Div m={{ b: '0.5em' }}>
											<Div className='addressDetails-addressname'>
												<Div
													flexGrow='1'
													minW='fit-content'>
													{EditInputMode({
														name: 'address_name',
														value:
															currentAddress?.data
																?.name,
														handleChange: handleAddressChange.bind(
															this,
															currentAddress.index,
															'name',
														),
													})}
												</Div>
												{currentAddress?.data?.name ==
													'Custom' && (
													<Input
														rounded='circle'
														value={
															currentAddress?.data
																?.name2
														}
														placeholder='Custom Name'
														borderColor={
															currentAddress?.data
																?.name2
																.length == 0
																? 'danger500'
																: 'gray700'
														}
														focusBorderColor={
															currentAddress?.data
																?.name2
																.length == 0
																? 'danger500'
																: 'gray700'
														}
														onChange={(e) =>
															handleAddressChange(
																currentAddress.index,
																'name2',
																e.target.value,
															)
														}
													/>
												)}
											</Div>
										</Div>
										<Div>
											{/* <Text minW='150px'>
												Address Value
											</Text>
											<Div flexGrow='1' m={{ x: '1em' }}>
												{EditInputMode({
													name: 'address_value',
													value:
														currentAddress?.data
															?.value,
													handleChange: handleAddressChange.bind(
														this,
														currentAddress.index,
														'value',
													),
												})}
											</Div> */}
											<Div className='address'>
												<Div className='addressDetails-section'>
													<Text className='addressDetails-type'>
														Address
													</Text>
													<Input
														className='addressDetails-input'
														placeholder='Complete Address'
														value={
															currentAddress.data
																?.value2
																?.address
														}
													/>
												</Div>
												<Div className='addressDetails-section'>
													<Text className='addressDetails-type'>
														City
													</Text>
													<Input
														className='addressDetails-input'
														placeholder='City'
														value={
															currentAddress.data
																?.value2?.city
														}
													/>
												</Div>
												<Div className='addressDetails-section'>
													<Text className='addressDetails-type'>
														State
													</Text>
													<Input
														className='addressDetails-input'
														placeholder='State'
														value={
															currentAddress.data
																?.value2?.state
														}
													/>
												</Div>
												<Div className='addressDetails-section'>
													<Text className='addressDetails-type'>
														Pincode
													</Text>
													<Input
														className='addressDetails-input'
														placeholder='Pincode'
														value={
															currentAddress.data
																?.value2
																?.pincode
														}
													/>
												</Div>
											</Div>
										</Div>
									</Div>
								</Div>
							</Div>
						</Div>
					)
				}
				popupFooter={
					currentAddress && (
						<>
							<Div flexGrow='1' className='modal-div-remover' />
							<Div d='flex' justify='flex-end' m={{ t: '1em' }}>
								<Button
									onClick={toggleAddressEditMode.bind(
										this,
										currentAddress.index,
										'discard',
									)}
									className='profile-section-edit-button-active modal-footer-secondary-button'
									m={{ y: '4px' }}
									p={{ x: '1.5em !important' }}
									h='fit-content'>
									DISCARD
								</Button>
								<Button
									onClick={toggleAddressEditMode.bind(
										this,
										currentAddress.index,
										'add-update',
									)}
									disabled={
										currentAddress.data.name == 'Custom' &&
										currentAddress.data.name2.length == 0
									}
									className='profile-section-edit-button modal-footer-primary-button'
									m={{ y: '4px' }}
									p={{ x: '1.5em !important' }}
									h='fit-content'>
									{addMode ? 'ADD' : 'UPDATE'}
								</Button>
							</Div>
						</>
					)
				}
			/>
		</ProfileSectionCard>
	);
};

export default function Profile() {
	const profileDetailsRef = useRef();
	const loginDetailsRef = useRef();
	const addressDetailsRef = useRef();
	const [userData, setUserData] = useState({
		profileData: [
			{
				name: 'Full Name',
				value: 'Karan Vadhwa',
			},
			{
				name: 'Date of birth',
				value: '22/09/1998',
			},
			{
				name: 'Gender',
				value: 'Male',
			},
		],
		loginData: [
			{
				name: 'Mobile No.',
				value: '+91-97111 54321',
			},
			{
				name: 'Email Id',
				value: 'karan.vadhwa@gmail.com',
			},
			{
				name: 'Password',
				value: '*************',
			},
		],
		addressData: [
			{
				name: 'Home',
				value: '51/20-A, DLF Phase-V, Gurugram, Haryana-110016',
				value2: {
					address: '51/20-A, DLF Phase-V',
					city: 'Gurugram',
					state: 'Haryana',
					pincode: '110016',
				},
			},
			{
				name: 'OFFICE',
				value: '35/80-D, IMT Industrial Area, Gurugram, Haryana-110025',
				value2: {
					address: '35/80-D, IMT Industrial Area',
					city: 'Gurugram',
					state: 'Haryana',
					pincode: '110025',
				},
			},
		],
	});
	const updateUserData = (type, data) => {
		let obj = userData;
		obj[type] = data;
		setUserData(obj);
	};
	return (
		<ThemeProvider theme={theme}>
			<Div className='profile-container'>
				<Div
					h='60px'
					m={{ b: '20px' }}
					className='profile-container-header'>
					<Link href='/marketplace'>
						<a>
							<Div
								h='60px'
								d='flex'
								align='center'
								w='fit-content'
								pos='absolute'>
								<Button
									rounded='circle'
									border='1px solid'
									borderColor='brand'
									bg='transparent'
									w='fit-content'
									h='fit-content'
									m='0px'
									p='2px'>
									<Icon
										name='LeftArrow'
										size='20px'
										color='brand'
									/>
								</Button>
								<Text
									textSize='16px'
									m={{ l: '1em' }}
									w='max-content'>
									Back to results
								</Text>
							</Div>
						</a>
					</Link>
					<Div
						h='60px'
						flexGrow='1'
						d='flex'
						justify='center'
						align='center'>
						<Text
							textSize='24px'
							textWeight='600'
							m={{ l: '1em' }}
							w='max-content'>
							My Profile
						</Text>
					</Div>
				</Div>
				<Div className='profile-left-right-container'>
					<Div className='profile-left-container'>
						{/* <Div d='flex' align='center' w='fit-content' h='60px'>
							<Button
								rounded='circle'
								border='1px solid'
								borderColor='brand'
								bg='transparent'
								w='fit-content'
								h='fit-content'
								m='0px'
								p='2px'>
								<Icon
									name='DownArrow'
									size='28px'
									color='brand'
								/>
							</Button>
							<Text
								textSize='24px'
								m={{ l: '1em' }}
								w='max-content'>
								My Profile
							</Text>
						</Div> */}
						<ProfileSettingCard
							profileDetailsRef={profileDetailsRef}
							loginDetailsRef={loginDetailsRef}
							addressDetailsRef={addressDetailsRef}
						/>
					</Div>
					<Div className='profile-right-container'>
						{/* <Div
							className='my-profile-container'
							d='flex'
							align='center'
							w='fit-content'
							h='60px'>
							<Button
								rounded='circle'
								border='1px solid'
								borderColor='brand'
								bg='transparent'
								w='fit-content'
								h='fit-content'
								m='0px'
								p='2px'>
								<Icon
									name='DownArrow'
									size='28px'
									color='brand'
								/>
							</Button>
							<Text
								textSize='24px'
								m={{ l: '1em' }}
								w='max-content'>
								My Profile
							</Text>
						</Div> */}
						{!!userData && (
							<>
								<ProfileDetails
									profileDetailsRef={profileDetailsRef}
									data={userData}
									updateUserData={updateUserData.bind(
										this,
										'profileData',
									)}
								/>
								<LoginDetails
									loginDetailsRef={loginDetailsRef}
									data={userData}
									updateUserData={updateUserData.bind(
										this,
										'loginData',
									)}
								/>
								<AddressDetails
									addressDetailsRef={addressDetailsRef}
									data={userData}
									updateUserData={updateUserData.bind(
										this,
										'addressData',
									)}
								/>
							</>
						)}
					</Div>
				</Div>
			</Div>
		</ThemeProvider>
	);
}
