import React, { useState, useEffect } from 'react';
import { Div, Input, Button, Text, Icon, Label, Dropdown } from 'atomize';
import OtpInput from 'react-otp-input';
import { useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import { loadLogoButton } from '../../Stores/pagestore';
import { updateTeknofeetStoreParam } from '../../Stores/teknofeetstore';
import Checkbox from '../../Components/common/checkbox';
import NotificationContainer from '../../Components/common/notificationcontainer';
import { InputType, ValidatePassword, CheckPasswordValidation } from '../../helpers/validation';
import { convertHMS, handleUserBrowserData } from '../../helpers';
import { generateOTP, verifyOTP, getClientDetails, getClientID, generateEmailOTP } from '../../helpers/apicalls';

const CommonContainer = (props) => {
	return (
		<Div className={`home-content-container ${props.noBackground ? 'no-background ' : ''} ${props.centerAlign ? 'center-text ' : ''}`}>
			{props.loader && (
				<Div className='loader-overlay'>
					<Div className='loader-icon loader-size' />
				</Div>
			)}
			{props.children}
		</Div>
	);
};
const CommonButton = (props) => {
	return (
		<Button
			className={props?.class}
			onClick={props.handleClick}
			bg='#1a89f0'
			hoverBg='white'
			hoverTextColor='#1a89f0'
			border='1px solid '
			borderColor='#1a89f0'
			rounded='circle'
			textSize='14px'
			// textWeight='600'
			h='40px'
			m={{ x: 'auto' }}
			w={props.fitContent ? 'fit-content' : '100%'}
			minW={props.minWidth ? props.minWidth : 'auto'}
			maxW='350px'>
			<Text h='100%' d='flex' flexDir='column' justify='center'>
				{props.name}
			</Text>
		</Button>
	);
};
const DetailsMenu = ({ handleClick }) => (
	<Div className='home-details-container'>
		<Div className='home-details-header'>
			<Text className='home-details-title'>“Keep Me Signed In” Checkbox</Text>
			<Button bg='transparent' h='fit-content' p='0px' onClick={handleClick}>
				<Icon hoverColor='danger700' name='Close' size='20px' />
			</Button>
		</Div>
		<Text className='home-details-content'>Choosing “Keep me signed in” reduces the number of times you’re asked to Sign-In on this device.</Text>
		<Text className='home-details-content'>To keep your account secure, use this option only on your personal devices.</Text>
	</Div>
);

const Login1 = (props) => {
	const router = useRouter();
	const [loading, setLoading] = useState(false);
	const handleLoginButton = async () => {
		setLoading(true);
		if (props.details.valueType) {
			const response = await getClientID({
				[props.details.valueType]: props.details.value,
			});
			if (response.success) {
				props.handleSetDetails('clientID', response.data.clientID);
				const response2 = await getClientDetails(response.data.clientID);
				if (response2.success) {
					props.handleFirstTime(false);
					props.setScreen('Login2');
				} else {
					props.handleFirstTime(true);
					props.setScreen('OTP');
				}
			} else {
				props.enableNotification({
					content: 'ACCOUNT DOES NOT EXISTS',
					primaryColor: 'info800',
					secondaryColor: 'info100',
				});
			}
		} else {
			props.enableNotification({
				content: props.details.value.length > 0 ? 'INCORRECT FORMAT' : 'NO INPUT',
				primaryColor: 'danger800',
				secondaryColor: 'danger100',
			});
		}
		setLoading(false);
	};
	return (
		<CommonContainer loader={props.loader} noBackground={true} centerAlign={true}>
			<Div className='upper-section'>
				<Div flexGrow='1' />
				<Text className='heading'>LOGIN WITH TEKNOFEET</Text>
			</Div>
			<Div className='middle-section'>
				<Input
					isLoading={loading}
					className='home-input'
					placeholder='Mobile no./Email ID'
					value={props.details.value || ''}
					onChange={(e) => {
						props.handleSetDetails('value', e.target.value);
					}}
					suffix={
						!loading && (
							<Div pos='absolute' top='0' right='0' d='flex'>
								<Button
									onClick={handleLoginButton.bind(this)}
									bg='#1a89f0'
									hoverBg='white'
									hoverTextColor='#1a89f0'
									border='1px solid '
									borderColor='#1a89f0'
									rounded='circle'
									h='25px'
									w='60px'
									m={{ y: '10px', r: '10px' }}>
									<Text h='100%' d='flex' flexDir='column' justify='center'>
										LOGIN
									</Text>
								</Button>
							</Div>
						)
					}
				/>
			</Div>
			<Div className='lower-section'>
				<Div flexGrow='1' />
				<Text className='heading'>NEW HERE ?</Text>
				<CommonButton
					name='CLICK HERE'
					class='home-click-here'
					fitContent={true}
					minWidth={'168px'}
					handleClick={() => {
						// props.setScreen('OTP');
						router.push('/about');
					}}
				/>
				<Div flexGrow='1' />
			</Div>
		</CommonContainer>
	);
};

const Login2 = (props) => {
	const dispatch = useDispatch();
	const router = useRouter();
	const [showPassword, setShowPassword] = useState(false);
	const [checked, setChecked] = useState(false);
	const [detailsContent, setDetailsContent] = useState(false);
	const [loading, setLoading] = useState(false);
	const handlePasswordSubmit = () => {
		setLoading(true);
		if (true) {
			handleUserBrowserData('STOREUSER', {
				value: props.details.value,
				password: props.details.password,
				valueType: props.details.valueType,
				timestamp: new Date().getTime(),
				status: checked ? 'AUTOLOGIN' : 'DEFAULT',
			});
			dispatch(updateTeknofeetStoreParam({ actionType: 'clientID', value: props.details.clientID }));
			props.enableNotification({
				content: 'LOGIN SUCCESS',
				primaryColor: 'success800',
				secondaryColor: 'success100',
			});

			setTimeout(() => {
				router.push('/marketplace');
			}, 1000);
		} else {
			props.enableNotification({
				content: 'PASSWORD MISMATCH',
				primaryColor: 'info800',
				secondaryColor: 'info100',
			});
		}
		setLoading(false);
	};
	return (
		<CommonContainer loader={props.loader}>
			<Div className='upper-section'>
				<Text className='heading'>Login</Text>
				<Text className='content' textWeight='bold'>
					{props.details.valueType == 'email' ? props.details.value || `abc@def.com` : `+91-${props.details.value || `123456789`}`}
					{/* +91-9560490133 */}
				</Text>
				<Div flexGrow='1' />
				<Text className='content color-3e3e3e' textWeight='bold'>
					Password
				</Text>
			</Div>
			<Div className='middle-section'>
				<Input
					isLoading={loading}
					className='home-input'
					placeholder='Password'
					type={showPassword ? 'text' : 'password'}
					onChange={(e) => {
						props.handleSetDetails('password', e.target.value);
					}}
					p={{ l: '1rem' }}
					suffix={
						!loading && (
							<Div pos='absolute' top='0' right='0' d='flex' m={{ r: '10px' }}>
								<Button onClick={() => setShowPassword(!showPassword)} bg='transparent' w='3rem' h='45px' rounded={{ r: 'md' }}>
									<Icon name={showPassword ? 'EyeSolid' : 'Eye'} color={showPassword ? 'gray900' : 'gray600'} size='16px' />
								</Button>
								<Button
									onClick={handlePasswordSubmit.bind(this)}
									bg='#1a89f0'
									hoverBg='white'
									hoverTextColor='#1a89f0'
									border='1px solid '
									borderColor='#1a89f0'
									rounded='circle'
									h='25px'
									w='60px'
									m={{ y: '10px' }}>
									LOGIN
								</Button>
							</Div>
						)
					}
				/>
			</Div>
			<Div className='lower-section'>
				<Div className='home-details'>
					<Label className='home-details-checkout color-3e3e3e'>
						<Checkbox
							checked={checked}
							handleChange={() => {
								setChecked(!checked);
							}}
							label='Keep me signed in.'
						/>
					</Label>
					<Dropdown
						className={detailsContent && 'home-details-dropdown'}
						textColor='#1a89f0'
						w='fit-content'
						isOpen={detailsContent}
						onClick={() => setDetailsContent(!detailsContent)}
						menu={
							<DetailsMenu
								handleClick={() => {
									setDetailsContent(false);
								}}
							/>
						}
						border='none'
						bg='transparent'
						hoverBg='transparent'
						focusBg='transparent'
						textWeight='bold'
						direction='topright'>
						Details
					</Dropdown>
				</Div>
				<Div className='or-divider'>or</Div>
				<CommonButton
					name='Reset password using OTP'
					handleClick={() => {
						props.setScreen('PasswordAssistance');
					}}
				/>
			</Div>
		</CommonContainer>
	);
};

const PasswordAssistance = (props) => {
	const handleVerify = async () => {
		if (props.details.valueType) {
			const response = await getClientID({
				[props.details.valueType]: props.details.value,
			});
			if (response.success) {
				const response2 = await getClientDetails(response.data.clientID);
				props.handleFirstTime(!response2.success);
				props.setScreen('OTP');
			} else {
				props.enableNotification({
					content: 'ACCOUNT DOES NOT EXISTS',
					primaryColor: 'info800',
					secondaryColor: 'info100',
				});
			}
		} else {
			alert('Incorrect Format');
		}
	};
	return (
		<CommonContainer loader={props.loader}>
			<Div className='upper-section'>
				<Text className='heading'>Password assistance</Text>
				<Text className='content'>Enter the email address or mobile phone number associated with your Truklay account.</Text>
				<Div flexGrow='1' />
			</Div>
			<Div className='middle-section'>
				<Input
					className='home-input'
					placeholder='Mobile no./Email ID'
					onChange={(e) => {
						props.handleSetDetails('value', e.target.value);
					}}
					value={props.details.value}
					suffix={
						<Div pos='absolute' top='0' right='0' d='flex'>
							<Button
								onClick={handleVerify.bind(this)}
								bg='#1a89f0'
								hoverBg='white'
								hoverTextColor='#1a89f0'
								border='1px solid '
								borderColor='#1a89f0'
								rounded='circle'
								h='25px'
								w='60px'
								m={{ y: '10px', r: '10px' }}>
								VERIFY
							</Button>
						</Div>
					}
				/>
			</Div>
			<Div className='lower-section'>
				<Div flexGrow='1' />
				<Text className='sub-heading'>Has your email address or mobile phone number changed?</Text>
				<Text className='sub-content'>
					If you no longer use the e-mail address associated with your Truklay account, you may contact{' '}
					<a
						className='home-text-link'
						style={{
							textDecoration: 'underline',
							fontWeight: 'bold',
						}}>
						Customer Service
					</a>{' '}
					for help restoring access to your account.
				</Text>

				<Button
					onClick={() => {
						props.setScreen('Login2');
					}}
					bg='transparent'
					textColor='#1a89f0'
					w='fit-content'
					m={{ x: 'auto' }}
					prefix={<Icon color='#1a89f0' name='LeftArrow' size='20px' />}
					suffix={<Icon color='transparent' name='LeftArrow' size='20px' />}>
					BACK
				</Button>
			</Div>
		</CommonContainer>
	);
};

const CreateNewPassword = (props) => {
	const [state, setState] = useState({
		password1: '',
		password2: '',
	});
	const [showPassword, setShowPassword] = useState(false);
	let showPasswordTimer = null;
	const handleChangePassword = (param, value) => {
		setState({
			...state,
			[param]: value,
		});
	};
	const handleSaveAndContinue = () => {
		if (state.password1 == state.password2) {
			if (CheckPasswordValidation(state.password1) && CheckPasswordValidation(state.password2)) {
				props.enableNotification({
					content: 'PASSWORD UPDATE SUCCESS',
					primaryColor: 'success800',
					secondaryColor: 'success100',
				});
			} else {
				props.enableNotification({
					content: 'PASSWORD UPDATE FAILED',
					primaryColor: 'danger800',
					secondaryColor: 'danger100',
				});
			}
		} else {
			props.enableNotification({
				content: 'PASSWORD DOES NOT MATCH',
				primaryColor: 'warning800',
				secondaryColor: 'warning100',
			});
		}
	};
	useEffect(() => {
		clearTimeout(showPasswordTimer);
		showPasswordTimer = setTimeout(() => {
			if (showPassword) {
				setShowPassword(false);
			}
		}, 1000);
	}, [showPassword]);
	return (
		<CommonContainer loader={props.loader}>
			<Div className='upper-section home-password'>
				<Text className='heading'>Create New Password</Text>
				<Text className='little-sub-content'>We’ll ask for this password whenever you sign in.</Text>
				<Div flexGrow='1' />
				<Input
					className='home-input2'
					placeholder='New Password'
					value={state.password1}
					onChange={(e) => {
						handleChangePassword('password1', e.target.value);
					}}
					type={showPassword ? 'text' : 'password'}
					suffix={
						<Div pos='absolute' top='0' right='0' d='flex' m={{ r: '10px' }}>
							<Button onClick={() => setShowPassword(!showPassword)} bg='transparent' w='3rem' h='45px' rounded={{ r: 'md' }}>
								<Icon name={showPassword ? 'EyeSolid' : 'Eye'} color={showPassword ? 'gray900' : 'gray600'} size='16px' />
							</Button>
						</Div>
					}
				/>
				<Div className='sub-content' d='flex' p={{ t: '0.5rem' }}>
					<Icon name='Info' size='16px' color='red' m={{ r: '0.5rem' }} style={{ height: '100%' }} />
					<span>Passwords must be at least 6 characters.</span>
				</Div>
			</Div>
			<Div className='middle-section home-password'>
				<Input
					className='home-input2'
					placeholder='Confirm Password'
					value={state.password2}
					onChange={(e) => {
						handleChangePassword('password2', e.target.value);
					}}
					type='password'
					m={{ b: '1rem' }}
				/>
				<Div m={{ b: '0.8rem' }}>
					<CommonButton name='SAVE & CONTINUE' fitContent={true} handleClick={handleSaveAndContinue.bind(this)} />
				</Div>
			</Div>
			<Div className='lower-section home-password'>
				{/* <Div flexGrow='1' /> */}
				<Text className='sub-heading'>Secure Password tips :</Text>
				<ul>
					<li>Use at least 8 characters, a combination of numbers and letters is best.</li>
					<li>Do not use the same password you have used with us previously.</li>
					<li>
						Do not use dictionary words, your name, e-mail address, mobile phone number or other personal information that can be easily
						obtained.
					</li>
					<li>Do not use the same password for multiple online accounts.</li>
				</ul>
			</Div>
		</CommonContainer>
	);
};

const OTP = (props) => {
	const [countDown, setCountDown] = useState(0);
	const [resendOTPcountDown, setResendOTPcountDown] = useState(0);
	const [otp, setOtp] = useState('');
	const [state, setState] = useState({
		session_id: null,
		email_otp: null,
	});
	let countDownTimer = null;
	let resendOTPcountDownTimer = null;
	const resetTimers = () => {
		clearTimeout(countDownTimer);
		clearTimeout(resendOTPcountDownTimer);
		setCountDown(180);
		setResendOTPcountDown(15);
	};
	const sendOTPMessage = async (phone_number) => {
		const response = await generateOTP(phone_number);
		if (response.success) {
			const { Details, Status } = response.data;
			if (Status == 'Success') {
				return Details;
			}
		}
		// return sendOTPMessage(phone_number);
	};
	const checkOTPMessage = async (phone_number, session_id, otp_input) => {
		const response = await verifyOTP(phone_number, session_id, otp_input);
		if (response.success) {
			const { Status, Details } = response.data;
			if (Status == 'Success' && Details == 'OTP Matched') {
				return true;
			}
		}
		return false;
	};
	const handleResendOTP = async () => {
		props.handleLoader(true);
		if (props.details.valueType == 'phone_number') {
			const session_id = await sendOTPMessage(props.details.value);
			setState({
				...state,
				session_id,
			});
		}
		props.handleLoader(false);
		resetTimers();
	};
	useEffect(() => {
		if (props.details.valueType == 'phone_number') {
			(async () => {
				const session_id = await sendOTPMessage(props.details.value);
				setState({
					...state,
					session_id,
				});
			})();
		}
		if (props.details.valueType == 'email') {
			(async () => {
				const email_otp = await generateEmailOTP(props.details.value);
				setState({
					...state,
					email_otp: email_otp.data.otp,
				});
			})();
		}
		setCountDown(180);
		setResendOTPcountDown(15);
	}, []);
	useEffect(() => {
		resendOTPcountDownTimer = setTimeout(() => {
			if (resendOTPcountDown > 0) {
				setResendOTPcountDown(resendOTPcountDown - 1);
			}
		}, 1000);
	}, [resendOTPcountDown]);
	useEffect(() => {
		countDownTimer = setTimeout(() => {
			if (countDown > 0) {
				setCountDown(countDown - 1);
			}
		}, 1000);
	}, [countDown]);
	useEffect(() => {
		if (otp.length == 6) {
			if (props.details.valueType == 'phone_number') {
				(async () => {
					props.handleLoader(true);
					const response = await checkOTPMessage(props.details.value, state.session_id, otp);
					props.handleLoader(false);
					if (response) {
						props.setScreen('CreateNewPassword');
					} else {
						alert('Does not match');
					}
				})();
			}
			if (props.details.valueType == 'email') {
				(async () => {
					props.handleLoader(true);
					props.handleLoader(false);
					if (otp == state.email_otp) {
						props.setScreen('CreateNewPassword');
					} else {
						alert('Does not match');
					}
				})();
			}
		}
	}, [otp]);
	return (
		<CommonContainer loader={props.loader}>
			<Div className='upper-section'>
				<Div d='flex' justify='space-between'>
					<Div>
						<Text className='heading' w='max-content'>
							Verify with OTP
						</Text>
						<Text className='content' textWeight='bold'>
							Sent to {props.details.value}
						</Text>
					</Div>
					<Div flexGrow='1' />
					<Div h='100%' flexGrow='1'>
						<Div className='otp-logo otp-size' />
					</Div>
				</Div>
				<Text className='content' textWeight='bold'>
					Enter OTP
				</Text>
			</Div>
			<Div className='middle-section'>
				<OtpInput
					// placeholder='******'
					isInputNum={true}
					value={otp}
					onChange={(otp) => {
						setOtp(otp);
					}}
					numInputs={6}
					containerStyle={{
						// height: '2rem'
						width: 'fit-content',
						marginBottom: '1rem',
					}}
					className='react-otp-input'
				/>
				<Div d='flex' w='100%' justify='space-between'>
					<Div d='flex'>
						<Text className='content' textWeight='bold'>
							<a
								className='home-text-link'
								onClick={handleResendOTP.bind(this)}
								style={
									resendOTPcountDown > 0
										? {
												pointerEvents: 'none',
												opacity: '0.8',
												color: '#76bdff !important',
												textDecoration: 'line-through',
										  }
										: {}
								}>
								RESEND OTP
							</a>
						</Text>
						{!!resendOTPcountDown && (
							<Text className='content' textColor='#ff584a !important' m={{ l: '0.5em' }} textWeight='bold'>
								{convertHMS(resendOTPcountDown)}
							</Text>
						)}
					</Div>
					{!!countDown && (
						<Text className='content' textColor='#ff584a !important' m={{ l: '0.5em' }} textWeight='bold'>
							{convertHMS(countDown)}
						</Text>
					)}
				</Div>
			</Div>
			<Div className='lower-section'>
				<Div flexGrow='1' />
				{!props.firstTime && (
					<>
						<Text className='content lighter-text' textWeight='bold'>
							Log-in using{' '}
							<a
								className='home-text-link'
								onClick={() => {
									props.setScreen('CreateNewPassword');
								}}>
								Password
							</a>
						</Text>
					</>
				)}
				<Text className='content lighter-text' textWeight='bold'>
					Having trouble logging in? <a className='home-text-link'>Get Help</a>
				</Text>
			</Div>
		</CommonContainer>
	);
};

export default function Home() {
	const dispatch = useDispatch();
	const router = useRouter();
	const [state, setState] = useState({
		screen: '',
		status: {
			backend: false,
			shopify: false,
		},
	});
	const [details, setDetails] = useState({
		value: '',
		valueType: null,
		password: '',
		otp: '',
		clientID: null,
	});
	const [loader, setLoader] = useState(false);
	const [firstTime, setFirstTime] = useState(false);
	const [notificationState, setNotificationState] = useState({
		isOpen: false,
		content: null,
		primaryColor: null,
		secondaryColor: null,
	});
	const enableNotification = ({ content, primaryColor, secondaryColor }) => {
		setNotificationState({
			...notificationState,
			isOpen: true,
			content,
			primaryColor,
			secondaryColor,
		});
	};
	const handleFirstTime = (val) => {
		console.log(firstTime);
		console.log(val);
		setFirstTime(val);
	};
	const handleLoader = (value) => {
		if (typeof value == 'boolean') {
			setLoader(value);
		} else {
			setLoader(!loader);
		}
	};
	const setScreen = (screen, screen2) => {
		setState({
			...state,
			screen: screen2 || screen,
		});
	};
	const handleSetDetails = (param, data) => {
		let tempDetails = {
			...details,
			[param]: data,
		};
		if (param == 'value') {
			tempDetails = { ...tempDetails, valueType: InputType(data) };
		}
		setDetails(tempDetails);
	};
	const handleCreateNewPassword = (password) => {
		return true;
	};
	useEffect(() => {
		if (router.pathname.includes('/home')) {
			(async () => {
				let teknofeetLoggedInUser = handleUserBrowserData('GETUSER');
				if (teknofeetLoggedInUser) {
					const { value, password, valueType, status } = teknofeetLoggedInUser;
					if (status == 'AUTOLOGIN') {
						const response = await getClientID({ [valueType]: value });
						if (response.success) {
							const response2 = await getClientDetails(response.data.clientID);
							if (response2.success) {
								router.push('/marketplace');
							}
						}
					}
					if (status != 'DEFAULT') {
						handleSetDetails('value', value);
					}
				}
				dispatch(loadLogoButton({ loadLogoButton: setScreen.bind(this, 'Login1') }));
				setTimeout(() => {
					setScreen('Login1');
				}, 1000);
			})();
		}
	}, [router]);
	useEffect(() => {
		if (notificationState.content && notificationState.isOpen == false) {
			setTimeout(() => {
				setNotificationState({
					...notificationState,
					content: null,
					primaryColor: null,
					secondaryColor: null,
				});
			}, 400);
		}
	}, [notificationState]);
	return (
		<>
			<Div className='home-content-outer-container'>
				{state.screen == 'Login1' && (
					<Login1
						setScreen={setScreen.bind(this)}
						loader={loader}
						handleLoader={handleLoader.bind(this)}
						details={details}
						handleSetDetails={handleSetDetails.bind(this)}
						handleFirstTime={(val) => {
							handleFirstTime(val);
						}}
						enableNotification={enableNotification.bind(this)}
					/>
				)}
				{state.screen == 'Login2' && (
					<Login2
						setScreen={setScreen.bind(this)}
						loader={loader}
						handleLoader={handleLoader.bind(this)}
						details={details}
						handleSetDetails={handleSetDetails.bind(this)}
						enableNotification={enableNotification.bind(this)}
					/>
				)}
				{state.screen == 'PasswordAssistance' && (
					<PasswordAssistance
						setScreen={setScreen.bind(this)}
						loader={loader}
						handleLoader={handleLoader.bind(this)}
						details={details}
						handleSetDetails={handleSetDetails.bind(this)}
						handleFirstTime={(val) => {
							handleFirstTime(val);
						}}
						enableNotification={enableNotification.bind(this)}
					/>
				)}
				{state.screen == 'CreateNewPassword' && (
					<CreateNewPassword
						setScreen={setScreen.bind(this)}
						loader={loader}
						handleLoader={handleLoader.bind(this)}
						enableNotification={enableNotification.bind(this)}
						handleCreateNewPassword={handleCreateNewPassword.bind(this)}
					/>
				)}
				{state.screen == 'OTP' && (
					<OTP
						setScreen={setScreen.bind(this)}
						firstTime={firstTime}
						loader={loader}
						handleLoader={handleLoader.bind(this)}
						details={details}
						handleSetDetails={handleSetDetails.bind(this)}
						enableNotification={enableNotification.bind(this)}
					/>
				)}
			</Div>
			<NotificationContainer
				primaryColor={notificationState.primaryColor}
				secondaryColor={notificationState.secondaryColor}
				isOpen={notificationState.isOpen}
				handleIsOpen={(value) => {
					setNotificationState({
						...notificationState,
						isOpen: value || false,
					});
				}}
				message={
					notificationState.content && (
						<>
							{/* LOGIN 1 */}
							{notificationState.content == 'ACCOUNT DOES NOT EXISTS' &&
								'Account does not exists, please register at the nearest Teknofeet outlet.'}
							{notificationState.content == 'INCORRECT FORMAT' && 'The format is incorrect.'}
							{notificationState.content == 'NO INPUT' && 'Please enter email / phone. no registered at the teknofeet outlet.'}
							{/* LOGIN 2 */}
							{notificationState.content == 'PASSWORD MISMATCH' && `Password doesn't match.`}
							{notificationState.content == 'LOGIN SUCCESS' && `Login Successful.`}
							{/* CREATE PASSWORD */}
							{notificationState.content == 'PASSWORD DOES NOT MATCH' && `Password does not match.`}
							{notificationState.content == 'PASSWORD UPDATE SUCCESS' &&
								`Password updated successfully. Please re-enter credentials to login.`}
							{notificationState.content == 'PASSWORD UPDATE FAILED' && `Password entered doesn't match the required criteria.`}
						</>
					)
				}
			/>
		</>
	);
}
