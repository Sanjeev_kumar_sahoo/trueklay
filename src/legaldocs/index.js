import React, { useState, useEffect } from 'react';

import { ThemeProvider, Div, Button, Text } from 'atomize';
import { useRouter } from 'next/router';

const theme = {
	colors: {
		brand: '#1a89f0',
	},
};

const CommonButton = ({ name, active, handleClick }) => {
	return (
		<Button
			className={
				active
					? 'legaldocs-top-nav-button legaldocs-top-nav-button-active'
					: 'legaldocs-top-nav-button'
			}
			onClick={handleClick}
			hoverShadow='2'>
			{name}
		</Button>
	);
};

const PrivacyPolicyContent = () => {
	return (
		<>
			<Div>
				<Div className='legaldocs-section-header'>
					TEKNOFEET PRIVACY POLICY
				</Div>
				<Div className='legaldocs-section-content'>
					<Text className='legaldocs-section-content-text'>
						Teknofeet is the registered entity under the domain of
						Truklay Global Pvt. Ltd.
					</Text>
					<Text className='legaldocs-section-content-text'>
						Effective January 1, 2020, Teknofeet (“Truklay,”
						“Teknofeet”, “we,” “us,” or “our”) has updated its
						Privacy Policy (“Policy”).
					</Text>
					<Text className='legaldocs-section-content-text'>
						Please read this Policy carefully. It is your choice
						whether to provide personal information to us. If you
						choose to do so, you are giving us your express consent
						to use, process, and disclose your personal information
						as set forth in this Policy. If you choose not to
						provide your personal information to us, you may not be
						able to use our services.
					</Text>
				</Div>
			</Div>
			<Div>
				<Div className='legaldocs-section-header'>Introduction</Div>
				<Div className='legaldocs-section-content'>
					<Text className='legaldocs-section-content-text'>
						We respect the privacy of our customers and users and
						have developed this Policy to demonstrate our commitment
						to protecting your privacy.
					</Text>
				</Div>
			</Div>
			<Div>
				<Div className='legaldocs-section-header'>
					1. Scope of this Policy
				</Div>
				<Div className='legaldocs-section-content'>
					<Text className='legaldocs-section-content-text'>
						This Policy is intended to describe our practices with
						respect to the collection, use, and maintenance of
						personal information relating to users of our websites
						(identified below), and our mobile or tablet
						applications (collectively, the “Apps”). For information
						about the data collected from Teknofeet foot scanning
						devices operated by third party retailers, please see
						Section 7 of this Policy below.
					</Text>
					<Text className='legaldocs-section-content-text'>
						For the purposes of this Policy, the term “Websites”
						shall refer collectively to www.teknofeet.com as well as
						the other websites that we operate and that link to this
						Policy, including:
						<br />
						<a>www.teknofeet.com</a>
						<br />
						<a>www.myfit.teknofeet.com</a>
						<br />
						<a>www.teknofeet.corporate.com/</a>
					</Text>
					<Text className='legaldocs-section-content-text'>
						In this Policy, personal information means information
						relating to an identified or identifiable natural
						person. An identifiable person is one who can be
						identified, directly or indirectly, by reference to an
						identified such as a name, an identification number,
						location data, and online identified or to one or more
						factors specific to his/her physical, physiological,
						genetic, mental, economic, cultural, or social identity.
						Personal information does not include aggregate
						information or de- identified information, meaning that
						the information can no longer be attributed to an
						identifiable natural person without the use of
						additional information.
					</Text>
					<Text className='legaldocs-section-content-text'>
						Personal information is required for us to be able to
						provide services to you. In addition, some of the
						services we provide require that we disclose your
						personal information to third parties, such as to
						companies that help us to provide those services. If you
						are not willing to provide your personal information and
						have it disclosed to third parties in accordance with
						this Policy, you will not be able to use our services.
					</Text>
					<Text className='legaldocs-section-content-text'>
						Our Websites and Apps may contain links to other
						websites that do not operate under this Policy. These
						third-party websites may independently solicit and
						collect personal information from you and, in some
						instances, provide us with information about your
						activities on those websites. We encourage you to review
						the privacy statements of all third-party websites you
						visit to understand their information practices.
					</Text>
					<Text className='legaldocs-section-content-text'>
						While we strongly support the protection of privacy on
						the Internet, we do not have control over, and cannot be
						responsible for, the actions of other third parties. We
						encourage you to review each such third party’s privacy
						statement and other otherwise understand its privacy
						practices, before providing personal information
						directly to it.
					</Text>
				</Div>
			</Div>
			<Div>
				<Div className='legaldocs-section-header'>
					2. Personal Information We Collect
				</Div>
				<Div className='legaldocs-section-content'>
					<Text className='legaldocs-section-content-text'>
						We collect personal information to operate effectively
						and provide you with the best experiences with our
						services. You provide some of this information directly,
						such as when you create an account, make a purchase, or
						contact us for support. We get some of your information,
						such as your IP address, by recording how you interact
						with our services. We may also get information about you
						from our business partners or other third parties.
					</Text>
					<Text className='legaldocs-section-content-text'>
						We may receive and collect certain personal information
						automatically, as outlined elsewhere in this Policy, and
						including analytics regarding our Websites and Apps,
						information your Internet browser automatically sends
						when you visit our Websites and Apps, and information
						collected by cookies. We may collect personal
						information that can identify you such as your name and
						email address, and other information that does not
						identify you. Irrespective of the fact if also you have
						registered yourself under DND or DNC or NCPR service,
						you still authorize us to give you a call from Teknofeet
						for the above mentioned purposes till 365 days of your
						registration with us
					</Text>
					<Text className='legaldocs-section-content-text'>
						<b>Information Provided by You</b>
						<br />
						We ask for and may collect personal information about
						you such as your name, address, e-mail address, height,
						weight, birth date and telephone number, and, if you
						make purchases from us, financial information such as
						your payment methods (credit card numbers or other
						financial information). We also may request information
						about your interests and activities, your gender and
						age, and other demographic information.
						<br />
						The most common of these circumstances where you would
						provide personal information include the following:
						<br />
						<b>i. Making a Purchase: </b>When you place an order
						online for one of our products, you may need to submit
						personal information to us.
						<br />
						<b>
							ii. Creating an account or getting the Universal
							FitPassport TM ID:
						</b>
						To use certain Apps and features of our Websites, or to
						make purchases, you may be required to create a
						password-protected user account and provide us with
						personal information. Similarly, when get your feet
						scanned to receive our Universal FitPassport TM ID, you
						may be asked to submit personal information as part of
						your profile generation process. We will store and use
						this information to administer the services which you
						choose to opt, and as permitted by this Policy.
						<br />
						<b>iii. Using Our Websites and Apps: </b>
						Our Websites and Apps may provide you with the ability
						to enter (either directly, or by authorizing us to
						access the information from a third party such as a
						social networking site or application) certain
						information such as your contacts, photos, or images you
						may submit to us to help us locate products for you.
						<br />
						<b>iv. Contacting Customer Service: </b>
						 When you contact our customer service, we may ask you
						to provide, or confirm, personal information so that we
						can better serve you.
						<br />
						<b>v. Entering a sweepstakes or contest: </b>
						If you enter a sweepstakes or contest, we offer, we may
						ask you to provide personal information so that we can
						consider your entry and, if you win, so that you may
						redeem your prize.
						<br />
						<b>vi. Interacting with social networking sites: </b>
						Our Websites and/or Apps may provide you with the
						ability to enter (directly, or by authorizing us to
						download the information from a third party such as a
						social networking site or application) personal
						information such as contacts or lists of friends. These
						or related applications may also allow you to provide
						information directly to social networking sites
						including information about your purchases, physical
						location, or comments. If you sign up to receive
						promotional communications or notifications from us, we
						may ask you to provide personal information so that we
						can provide these to you.
						<br />
						<b>
							Information that We Collect from You on Our Websites
							and Apps
						</b>
						<br />
						We also may use various technologies to collect
						information from your computer or device and about your
						activities on our Websites or Apps.
						<br />
						<b>i. Information collected automatically. </b>We may
						automatically collect information from you when you
						visit our Websites and Apps. This information may
						include your IP address, your browser type and language,
						access times, the content of any undeleted cookies that
						your browser previously accepted from us, referring or
						exit website address, internet service provider,
						date/time stamp, operating system, locale and language
						preferences, and system configuration information.
						<br />
						<b>ii. Social Media. </b>We may provide you the ability
						to sign into any of our Truklay Websites or App using a
						social media account such as a Facebook, Twitter,
						Instagram, or Google account, or use your social media
						accounts to share information from our Websites or Apps.
						In these cases, as well as when you use an Teknofeet App
						on a social media site or choose to join (or
						&quot;like&quot;) an Teknofeet page on a social media
						site, the social media site may provide us with certain
						information about you, including but not limited to your
						contacts and other &quot;likes&quot;. In addition, we
						may share your information, including personal
						information, with third party vendors to facilitate your
						choice to sign into Teknofeet Website or App using a
						social media account. Please see the privacy policy for
						the applicable social media site to learn more about how
						these sites share your personal information.
						<br />
						<b>iii. Cookies. </b>When you visit our Websites and/or
						Apps, we may assign your computer one or more cookies to
						facilitate access to our site and to personalize your
						online experience. Using a cookie, we also may
						automatically collect information about your online
						activity on our site, such as the web pages you visit,
						the links you click, and the searches you conduct on our
						site. Most browsers automatically accept cookies, but
						you can usually modify your browser setting to decline
						cookies. If you choose to decline cookies, please note
						that you may not be able to sign in or use some of the
						interactive features offered on our Websites or Apps. A
						cookie is a small text file that is stored on a
						user&#39;s computer for record keeping purposes. Cookies
						can be either session cookies or persistent cookies. A
						session cookie expires when you close your browser and
						is used to make it easier for you to navigate our
						website. A persistent cookie remains on your hard drive
						for an extended period. For example, when you sign in to
						our Websites or Apps, we will record your user or member
						ID, which is your email address or a 10figure ID, and
						the name on your user or member account in the cookie
						file on your computer. We store your unique member ID in
						a cookie for automatic sign-in. This cookie is removed
						when you sign-out. For security purposes, we will
						encrypt the unique member ID and any other user or
						member account-related data that we store in such
						cookies. In the case of sites and services that do not
						use a user or member ID, the cookie will contain a
						unique identifier. We may allow our authorized service
						providers to serve cookies from our website to allow
						them to assist us in various activities, such as doing
						analysis and research on the effectiveness of our site,
						content, and advertising. You may delete or decline
						cookies by changing your browser settings (click
						&quot;Help&quot; in the toolbar of most browsers for
						instructions). If you do so, some of the features and
						services of our Websites and/or Apps may not function
						properly.
						<br />
						<b>iv. Other technologies. </b>We may use standard
						Internet technology, such as web beacons and other
						similar technologies, to track your use of our Websites
						and Apps. We also may include web beacons in promotional
						e-mail messages or newsletters to determine whether
						messages have been opened and acted upon. The
						information we obtain in this manner enables us to
						customize the services we offer to users of our Websites
						and Apps to deliver targeted advertisements and to
						measure the overall effectiveness of our online
						advertising, content, programming, or other activities.
						Web beacons (also known as clear gifs, pixel tags or web
						bugs) are tiny graphics with a unique identifier,
						similar in function to cookies, and are used to track
						the online movements of web users or to access cookies.
						Unlike cookies, which are stored on the user&#39;s
						computer hard drive, web beacons are embedded invisibly
						on the web pages (or in email) and are about the size of
						the period at the end of this sentence. Web beacons may
						be used to deliver or communicate with cookies, to count
						users who have visited certain pages and to understand
						usage patterns. We also may receive an anonymous
						identification number if you come to our site from an
						online advertisement displayed on a third- party
						website.
						<br />
						<b>Information Collected From Other Sources</b>
						<br />
						We may also obtain both personal and non-personal
						information about you from advertising companies, and ad
						networks business partners, contractors and other third
						parties and add it to our account information or other
						information we have collected. Examples of such
						information that we may receive include: updated
						delivery and address information, purchase history, and
						additional demographic information. We may combine this
						information with information we collect through our
						Websites and Apps or from other sources.
					</Text>
				</Div>
			</Div>
			<Div>
				<Div className='legaldocs-section-header'>
					3. How We Use Personal Information
				</Div>
				<Div className='legaldocs-section-content'>
					<Text className='legaldocs-section-content-text'>
						We respect the privacy of our customers and users and
						have developed this Policy to demonstrate our commitment
						to protecting your privacy.
					</Text>
				</Div>
			</Div>
		</>
	);
};
const LegalDisclaimerContent = () => {
	return <>LEGAL DISCLAIMER</>;
};
const TermsConditionContent = () => {
	return <>TERMS & CONDITIONS</>;
};

const LegalDocs = () => {
	const router = useRouter();
	const [state, setState] = useState({
		mode: null,
	});
	const handleClick = (value) => {
		router.push({
			pathname: router.pathname,
			query: {
				docs: value,
			},
		});
	};
	useEffect(() => {
		if (router.asPath.includes('/legaldocs?')) {
			if (router.query?.docs) {
				switch (router.query.docs) {
					case 'PRIVACY POLICY':
						setState({ ...state, mode: 'PRIVACY POLICY' });
						break;
					case 'LEGAL DISCLAIMER':
						setState({ ...state, mode: 'LEGAL DISCLAIMER' });
						break;
					case 'TERMS CONDITIONS':
						setState({ ...state, mode: 'TERMS CONDITIONS' });
						break;
					default:
						router.push({
							pathname: router.pathname,
							query: {
								docs: 'PRIVACY POLICY',
							},
						});
						break;
				}
			}
		} else {
			router.push({
				pathname: router.pathname,
				query: {
					docs: 'PRIVACY POLICY',
				},
			});
		}
	}, [router]);
	return (
		<ThemeProvider theme={theme}>
			<Div className='legaldocs-container'>
				<Div className='legaldocs-top-nav'>
					<CommonButton
						name='PRIVACY POLICY'
						handleClick={handleClick.bind(this, 'PRIVACY POLICY')}
						active={state.mode == 'PRIVACY POLICY'}
					/>
					<CommonButton
						name='LEGAL DISCLAIMER'
						handleClick={handleClick.bind(this, 'LEGAL DISCLAIMER')}
						active={state.mode == 'LEGAL DISCLAIMER'}
					/>
					<CommonButton
						name='TERMS & CONDITIONS'
						handleClick={handleClick.bind(this, 'TERMS CONDITIONS')}
						active={state.mode == 'TERMS CONDITIONS'}
					/>
				</Div>
				<Div className='legaldocs-section'>
					{state.mode == 'PRIVACY POLICY' && <PrivacyPolicyContent />}
					{state.mode == 'LEGAL DISCLAIMER' && (
						<LegalDisclaimerContent />
					)}
					{state.mode == 'TERMS CONDITIONS' && (
						<TermsConditionContent />
					)}
				</Div>
			</Div>
		</ThemeProvider>
	);
};

export default LegalDocs;
